export const data = [
  {
    date: '14/01/2010',
    volume: 108223500,
    close: 28.33,
  },
  {
    date: '15/01/2010',
    volume: 148516900,
    close: 27.86,
  },
  {
    date: '19/01/2010',
    volume: 182501900,
    close: 29.09,
  },
  {
    date: '20/01/2010',
    volume: 153038200,
    close: 28.64,
  },
  {
    date: '21/01/2010',
    volume: 152038600,
    close: 28.15,
  },
  {
    date: '22/01/2010',
    volume: 220441900,
    close: 26.75,
  },
  {
    date: '25/01/2010',
    volume: 266424900,
    close: 27.47,
  },
  {
    date: '26/01/2010',
    volume: 466777500,
    close: 27.86,
  },
  {
    date: '27/01/2010',
    volume: 430642100,
    close: 28.12,
  },
  {
    date: '28/01/2010',
    volume: 293375600,
    close: 26.96,
  },
  {
    date: '29/01/2010',
    volume: 311488100,
    close: 25.98,
  },
  {
    date: '01/02/2010',
    volume: 187469100,
    close: 26.34,
  },
  {
    date: '02/02/2010',
    volume: 174585600,
    close: 26.5,
  },
  {
    date: '03/02/2010',
    volume: 153832000,
    close: 26.95,
  },
  {
    date: '04/02/2010',
    volume: 189413000,
    close: 25.98,
  },
  {
    date: '05/02/2010',
    volume: 212576700,
    close: 26.44,
  },
  {
    date: '08/02/2010',
    volume: 119567700,
    close: 26.26,
  },
  {
    date: '09/02/2010',
    volume: 158221700,
    close: 26.54,
  },
  {
    date: '10/02/2010',
    volume: 92590400,
    close: 26.4,
  },
  {
    date: '11/02/2010',
    volume: 137586400,
    close: 26.88,
  },
  {
    date: '12/02/2010',
    volume: 163867200,
    close: 27.11,
  },
  {
    date: '16/02/2010',
    volume: 135934400,
    close: 27.52,
  },
  {
    date: '17/02/2010',
    volume: 109099200,
    close: 27.4,
  },
  {
    date: '18/02/2010',
    volume: 105706300,
    close: 27.45,
  },
  {
    date: '19/02/2010',
    volume: 103867400,
    close: 27.28,
  },
  {
    date: '22/02/2010',
    volume: 97640900,
    close: 27.11,
  },
  {
    date: '23/02/2010',
    volume: 143773700,
    close: 26.66,
  },
  {
    date: '24/02/2010',
    volume: 115141600,
    close: 27.15,
  },
  {
    date: '25/02/2010',
    volume: 166281500,
    close: 27.33,
  },
  {
    date: '26/02/2010',
    volume: 126865200,
    close: 27.68,
  },
  {
    date: '01/03/2010',
    volume: 137523400,
    close: 28.27,
  },
  {
    date: '02/03/2010',
    volume: 141636600,
    close: 28.25,
  },
  {
    date: '03/03/2010',
    volume: 93013200,
    close: 28.32,
  },
  {
    date: '04/03/2010',
    volume: 91510300,
    close: 28.51,
  },
  {
    date: '05/03/2010',
    volume: 224905100,
    close: 29.62,
  },
  {
    date: '08/03/2010',
    volume: 107472400,
    close: 29.64,
  },
  {
    date: '09/03/2010',
    volume: 230064800,
    close: 30.17,
  },
  {
    date: '10/03/2010',
    volume: 149054500,
    close: 30.42,
  },
  {
    date: '11/03/2010',
    volume: 101425100,
    close: 30.51,
  },
  {
    date: '12/03/2010',
    volume: 104080900,
    close: 30.66,
  },
  {
    date: '15/03/2010',
    volume: 123375700,
    close: 30.28,
  },
  {
    date: '16/03/2010',
    volume: 111727000,
    close: 30.36,
  },
  {
    date: '17/03/2010',
    volume: 112739200,
    close: 30.32,
  },
  {
    date: '18/03/2010',
    volume: 85527400,
    close: 30.39,
  },
  {
    date: '19/03/2010',
    volume: 139861400,
    close: 30.07,
  },
  {
    date: '22/03/2010',
    volume: 114104900,
    close: 30.4,
  },
  {
    date: '23/03/2010',
    volume: 150607800,
    close: 30.89,
  },
  {
    date: '24/03/2010',
    volume: 149445100,
    close: 31.03,
  },
  {
    date: '25/03/2010',
    volume: 135571100,
    close: 30.66,
  },
  {
    date: '26/03/2010',
    volume: 160218800,
    close: 31.24,
  },
  {
    date: '29/03/2010',
    volume: 135186100,
    close: 31.44,
  },
  {
    date: '30/03/2010',
    volume: 131827500,
    close: 31.91,
  },
  {
    date: '31/03/2010',
    volume: 107664900,
    close: 31.79,
  },
  {
    date: '01/04/2010',
    volume: 150786300,
    close: 31.92,
  },
  {
    date: '05/04/2010',
    volume: 171126900,
    close: 32.26,
  },
  {
    date: '06/04/2010',
    volume: 111754300,
    close: 32.41,
  },
  {
    date: '07/04/2010',
    volume: 157125500,
    close: 32.55,
  },
  {
    date: '08/04/2010',
    volume: 143247300,
    close: 32.46,
  },
  {
    date: '09/04/2010',
    volume: 83545700,
    close: 32.71,
  },
  {
    date: '12/04/2010',
    volume: 83256600,
    close: 32.78,
  },
  {
    date: '13/04/2010',
    volume: 76552700,
    close: 32.8,
  },
  {
    date: '14/04/2010',
    volume: 101019100,
    close: 33.24,
  },
  {
    date: '15/04/2010',
    volume: 94196200,
    close: 33.67,
  },
  {
    date: '16/04/2010',
    volume: 187636400,
    close: 33.47,
  },
  {
    date: '19/04/2010',
    volume: 141731100,
    close: 33.42,
  },
  {
    date: '20/04/2010',
    volume: 184581600,
    close: 33.09,
  },
  {
    date: '21/04/2010',
    volume: 245597800,
    close: 35.07,
  },
  {
    date: '22/04/2010',
    volume: 198356200,
    close: 36.05,
  },
  {
    date: '23/04/2010',
    volume: 199238900,
    close: 36.64,
  },
  {
    date: '26/04/2010',
    volume: 119767200,
    close: 36.46,
  },
  {
    date: '27/04/2010',
    volume: 177335900,
    close: 35.45,
  },
  {
    date: '28/04/2010',
    volume: 189600600,
    close: 35.39,
  },
  {
    date: '29/04/2010',
    volume: 139710200,
    close: 36.34,
  },
  {
    date: '30/04/2010',
    volume: 135615900,
    close: 35.32,
  },
  {
    date: '03/05/2010',
    volume: 113585500,
    close: 36.03,
  },
  {
    date: '04/05/2010',
    volume: 180954900,
    close: 34.99,
  },
  {
    date: '05/05/2010',
    volume: 220775800,
    close: 34.63,
  },
  {
    date: '06/05/2010',
    volume: 321465200,
    close: 33.31,
  },
  {
    date: '07/05/2010',
    volume: 419004600,
    close: 31.91,
  },
  {
    date: '10/05/2010',
    volume: 246076600,
    close: 34.36,
  },
  {
    date: '11/05/2010',
    volume: 212226700,
    close: 34.7,
  },
  {
    date: '12/05/2010',
    volume: 163594900,
    close: 35.46,
  },
  {
    date: '13/05/2010',
    volume: 149928100,
    close: 34.95,
  },
  {
    date: '14/05/2010',
    volume: 189840700,
    close: 34.34,
  },
  {
    date: '17/05/2010',
    volume: 190708700,
    close: 34.39,
  },
  {
    date: '18/05/2010',
    volume: 195669600,
    close: 34.14,
  },
  {
    date: '19/05/2010',
    volume: 256431700,
    close: 33.6,
  },
  {
    date: '20/05/2010',
    volume: 320728800,
    close: 32.16,
  },
  {
    date: '21/05/2010',
    volume: 305972800,
    close: 32.78,
  },
  {
    date: '24/05/2010',
    volume: 188559700,
    close: 33.38,
  },
  {
    date: '25/05/2010',
    volume: 262001600,
    close: 33.17,
  },
  {
    date: '26/05/2010',
    volume: 212663500,
    close: 33.02,
  },
  {
    date: '27/05/2010',
    volume: 166570600,
    close: 34.27,
  },
  {
    date: '28/05/2010',
    volume: 203903700,
    close: 34.75,
  },
  {
    date: '01/06/2010',
    volume: 219118200,
    close: 35.29,
  },
  {
    date: '02/06/2010',
    volume: 172137000,
    close: 35.71,
  },
  {
    date: '03/06/2010',
    volume: 162526700,
    close: 35.6,
  },
  {
    date: '04/06/2010',
    volume: 189576100,
    close: 34.63,
  },
  {
    date: '07/06/2010',
    volume: 221735500,
    close: 33.95,
  },
  {
    date: '08/06/2010',
    volume: 250192600,
    close: 33.73,
  },
  {
    date: '09/06/2010',
    volume: 213657500,
    close: 32.9,
  },
  {
    date: '10/06/2010',
    volume: 194089000,
    close: 33.89,
  },
  {
    date: '11/06/2010',
    volume: 136439800,
    close: 34.3,
  },
  {
    date: '14/06/2010',
    volume: 150740100,
    close: 34.4,
  },
  {
    date: '15/06/2010',
    volume: 146268500,
    close: 35.13,
  },
  {
    date: '16/06/2010',
    volume: 195919500,
    close: 36.15,
  },
  {
    date: '17/06/2010',
    volume: 218213800,
    close: 36.78,
  },
  {
    date: '18/06/2010',
    volume: 196155400,
    close: 37.08,
  },
  {
    date: '21/06/2010',
    volume: 194122600,
    close: 36.55,
  },
  {
    date: '22/06/2010',
    volume: 179315500,
    close: 37.05,
  },
  {
    date: '23/06/2010',
    volume: 192114300,
    close: 36.66,
  },
  {
    date: '24/06/2010',
    volume: 178569300,
    close: 36.39,
  },
  {
    date: '25/06/2010',
    volume: 137485600,
    close: 36.08,
  },
  {
    date: '28/06/2010',
    volume: 146237000,
    close: 36.3,
  },
  {
    date: '29/06/2010',
    volume: 283336200,
    close: 34.66,
  },
  {
    date: '30/06/2010',
    volume: 184863000,
    close: 34.03,
  },
  {
    date: '01/07/2010',
    volume: 255724000,
    close: 33.62,
  },
  {
    date: '02/07/2010',
    volume: 173460700,
    close: 33.41,
  },
  {
    date: '06/07/2010',
    volume: 153808900,
    close: 33.64,
  },
  {
    date: '07/07/2010',
    volume: 163639000,
    close: 34.99,
  },
  {
    date: '08/07/2010',
    volume: 184536100,
    close: 34.92,
  },
  {
    date: '09/07/2010',
    volume: 108330600,
    close: 35.12,
  },
  {
    date: '12/07/2010',
    volume: 140719600,
    close: 34.81,
  },
  {
    date: '13/07/2010',
    volume: 297731000,
    close: 34.06,
  },
  {
    date: '14/07/2010',
    volume: 203011900,
    close: 34.19,
  },
  {
    date: '15/07/2010',
    volume: 206216500,
    close: 34.02,
  },
  {
    date: '16/07/2010',
    volume: 259964600,
    close: 33.81,
  },
  {
    date: '19/07/2010',
    volume: 256119500,
    close: 33.22,
  },
  {
    date: '20/07/2010',
    volume: 268737700,
    close: 34.08,
  },
  {
    date: '21/07/2010',
    volume: 296417800,
    close: 34.39,
  },
  {
    date: '22/07/2010',
    volume: 161329700,
    close: 35.04,
  },
  {
    date: '23/07/2010',
    volume: 133347200,
    close: 35.17,
  },
  {
    date: '26/07/2010',
    volume: 105137900,
    close: 35.08,
  },
  {
    date: '27/07/2010',
    volume: 146192900,
    close: 35.73,
  },
  {
    date: '28/07/2010',
    volume: 129996300,
    close: 35.3,
  },
  {
    date: '29/07/2010',
    volume: 160951700,
    close: 34.92,
  },
  {
    date: '30/07/2010',
    volume: 112052500,
    close: 34.8,
  },
  {
    date: '02/08/2010',
    volume: 107013900,
    close: 35.42,
  },
  {
    date: '03/08/2010',
    volume: 104413400,
    close: 35.43,
  },
  {
    date: '04/08/2010',
    volume: 105093800,
    close: 35.58,
  },
  {
    date: '05/08/2010',
    volume: 72274300,
    close: 35.4,
  },
  {
    date: '06/08/2010',
    volume: 111224400,
    close: 35.19,
  },
  {
    date: '09/08/2010',
    volume: 75782000,
    close: 35.41,
  },
  {
    date: '10/08/2010',
    volume: 112980000,
    close: 35.09,
  },
  {
    date: '11/08/2010',
    volume: 155013600,
    close: 33.85,
  },
  {
    date: '12/08/2010',
    volume: 133730100,
    close: 34.06,
  },
  {
    date: '13/08/2010',
    volume: 88717300,
    close: 33.7,
  },
  {
    date: '16/08/2010',
    volume: 79607500,
    close: 33.5,
  },
  {
    date: '17/08/2010',
    volume: 105660100,
    close: 34.09,
  },
  {
    date: '18/08/2010',
    volume: 84924000,
    close: 34.24,
  },
  {
    date: '19/08/2010',
    volume: 106676500,
    close: 33.8,
  },
  {
    date: '20/08/2010',
    volume: 96057500,
    close: 33.77,
  },
  {
    date: '23/08/2010',
    volume: 103510400,
    close: 33.25,
  },
  {
    date: '24/08/2010',
    volume: 150641400,
    close: 32.46,
  },
  {
    date: '25/08/2010',
    volume: 149216900,
    close: 32.86,
  },
  {
    date: '26/08/2010',
    volume: 116626300,
    close: 32.51,
  },
  {
    date: '27/08/2010',
    volume: 137097800,
    close: 32.69,
  },
  {
    date: '30/08/2010',
    volume: 95822300,
    close: 32.81,
  },
  {
    date: '31/08/2010',
    volume: 105196700,
    close: 32.89,
  },
  {
    date: '01/09/2010',
    volume: 174259400,
    close: 33.87,
  },
  {
    date: '02/09/2010',
    volume: 103856900,
    close: 34.11,
  },
  {
    date: '03/09/2010',
    volume: 130197200,
    close: 35.01,
  },
  {
    date: '07/09/2010',
    volume: 85639400,
    close: 34.88,
  },
  {
    date: '08/09/2010',
    volume: 131637800,
    close: 35.57,
  },
  {
    date: '09/09/2010',
    volume: 109643800,
    close: 35.59,
  },
  {
    date: '10/09/2010',
    volume: 96885600,
    close: 35.63,
  },
  {
    date: '13/09/2010',
    volume: 97195000,
    close: 36.13,
  },
  {
    date: '14/09/2010',
    volume: 102037600,
    close: 36.26,
  },
  {
    date: '15/09/2010',
    volume: 107342200,
    close: 36.56,
  },
  {
    date: '16/09/2010',
    volume: 163025800,
    close: 37.42,
  },
  {
    date: '17/09/2010',
    volume: 158619300,
    close: 37.25,
  },
  {
    date: '20/09/2010',
    volume: 164669400,
    close: 38.32,
  },
  {
    date: '21/09/2010',
    volume: 167018600,
    close: 38.39,
  },
  {
    date: '22/09/2010',
    volume: 146322400,
    close: 38.93,
  },
  {
    date: '23/09/2010',
    volume: 196529200,
    close: 39.09,
  },
  {
    date: '24/09/2010',
    volume: 162372000,
    close: 39.55,
  },
  {
    date: '27/09/2010',
    volume: 120708700,
    close: 39.39,
  },
  {
    date: '28/09/2010',
    volume: 258760600,
    close: 38.81,
  },
  {
    date: '29/09/2010',
    volume: 117411000,
    close: 38.88,
  },
  {
    date: '30/09/2010',
    volume: 168347900,
    close: 38.39,
  },
  {
    date: '01/10/2010',
    volume: 112035700,
    close: 38.22,
  },
  {
    date: '04/10/2010',
    volume: 108825500,
    close: 37.7,
  },
  {
    date: '05/10/2010',
    volume: 125491800,
    close: 39.09,
  },
  {
    date: '06/10/2010',
    volume: 167717200,
    close: 39.12,
  },
  {
    date: '07/10/2010',
    volume: 102099900,
    close: 39.13,
  },
  {
    date: '08/10/2010',
    volume: 164600800,
    close: 39.78,
  },
  {
    date: '11/10/2010',
    volume: 106938300,
    close: 39.96,
  },
  {
    date: '12/10/2010',
    volume: 139636000,
    close: 40.39,
  },
  {
    date: '13/10/2010',
    volume: 157523100,
    close: 40.6,
  },
  {
    date: '14/10/2010',
    volume: 108824100,
    close: 40.9,
  },
  {
    date: '15/10/2010',
    volume: 230548500,
    close: 42.58,
  },
  {
    date: '18/10/2010',
    volume: 273252700,
    close: 43.02,
  },
  {
    date: '19/10/2010',
    volume: 308196000,
    close: 41.87,
  },
  {
    date: '20/10/2010',
    volume: 180406100,
    close: 42.01,
  },
  {
    date: '21/10/2010',
    volume: 137865000,
    close: 41.87,
  },
  {
    date: '22/10/2010',
    volume: 93194500,
    close: 41.6,
  },
  {
    date: '25/10/2010',
    volume: 98115500,
    close: 41.78,
  },
  {
    date: '26/10/2010',
    volume: 98232400,
    close: 41.67,
  },
  {
    date: '27/10/2010',
    volume: 99750700,
    close: 41.64,
  },
  {
    date: '28/10/2010',
    volume: 137762800,
    close: 41.29,
  },
  {
    date: '29/10/2010',
    volume: 107627800,
    close: 40.72,
  },
  {
    date: '01/11/2010',
    volume: 105972300,
    close: 41.15,
  },
  {
    date: '02/11/2010',
    volume: 108482500,
    close: 41.85,
  },
  {
    date: '03/11/2010',
    volume: 127087100,
    close: 42.32,
  },
  {
    date: '04/11/2010',
    volume: 160622000,
    close: 43.06,
  },
  {
    date: '05/11/2010',
    volume: 90313300,
    close: 42.9,
  },
  {
    date: '08/11/2010',
    volume: 70439600,
    close: 43.1,
  },
  {
    date: '09/11/2010',
    volume: 95886000,
    close: 42.76,
  },
  {
    date: '10/11/2010',
    volume: 96056800,
    close: 43.02,
  },
  {
    date: '11/11/2010',
    volume: 90321000,
    close: 42.84,
  },
  {
    date: '12/11/2010',
    volume: 198961700,
    close: 41.67,
  },
  {
    date: '15/11/2010',
    volume: 100901500,
    close: 41.54,
  },
  {
    date: '16/11/2010',
    volume: 164412500,
    close: 40.8,
  },
  {
    date: '17/11/2010',
    volume: 119862400,
    close: 40.65,
  },
  {
    date: '18/11/2010',
    volume: 123622800,
    close: 41.73,
  },
  {
    date: '19/11/2010',
    volume: 96210800,
    close: 41.5,
  },
  {
    date: '22/11/2010',
    volume: 98268800,
    close: 42.39,
  },
  {
    date: '23/11/2010',
    volume: 129861900,
    close: 41.77,
  },
  {
    date: '24/11/2010',
    volume: 103431300,
    close: 42.59,
  },
  {
    date: '26/11/2010',
    volume: 59396400,
    close: 42.61,
  },
  {
    date: '29/11/2010',
    volume: 111446300,
    close: 42.87,
  },
  {
    date: '30/11/2010',
    volume: 125464500,
    close: 42.09,
  },
  {
    date: '01/12/2010',
    volume: 115437700,
    close: 42.8,
  },
  {
    date: '02/12/2010',
    volume: 115709300,
    close: 43.04,
  },
  {
    date: '03/12/2010',
    volume: 85523200,
    close: 42.94,
  },
  {
    date: '06/12/2010',
    volume: 112120400,
    close: 43.31,
  },
  {
    date: '07/12/2010',
    volume: 97863500,
    close: 43.05,
  },
  {
    date: '08/12/2010',
    volume: 80483900,
    close: 43.43,
  },
  {
    date: '09/12/2010',
    volume: 73537800,
    close: 43.26,
  },
  {
    date: '10/12/2010',
    volume: 65627800,
    close: 43.37,
  },
  {
    date: '13/12/2010',
    volume: 109953900,
    close: 43.52,
  },
  {
    date: '14/12/2010',
    volume: 87752000,
    close: 43.33,
  },
  {
    date: '15/12/2010',
    volume: 104328000,
    close: 43.34,
  },
  {
    date: '16/12/2010',
    volume: 80507700,
    close: 43.46,
  },
  {
    date: '17/12/2010',
    volume: 96732300,
    close: 43.37,
  },
  {
    date: '20/12/2010',
    volume: 96402600,
    close: 43.59,
  },
  {
    date: '21/12/2010',
    volume: 64088500,
    close: 43.86,
  },
  {
    date: '22/12/2010',
    volume: 66480400,
    close: 43.99,
  },
  {
    date: '23/12/2010',
    volume: 55789300,
    close: 43.78,
  },
  {
    date: '27/12/2010',
    volume: 62454000,
    close: 43.92,
  },
  {
    date: '28/12/2010',
    volume: 43981000,
    close: 44.03,
  },
  {
    date: '29/12/2010',
    volume: 40784800,
    close: 44.01,
  },
  {
    date: '30/12/2010',
    volume: 39373600,
    close: 43.79,
  },
  {
    date: '31/12/2010',
    volume: 48377000,
    close: 43.64,
  },
  {
    date: '03/01/2011',
    volume: 111284600,
    close: 44.59,
  },
  {
    date: '04/01/2011',
    volume: 77270200,
    close: 44.82,
  },
  {
    date: '05/01/2011',
    volume: 63879900,
    close: 45.18,
  },
  {
    date: '06/01/2011',
    volume: 75107200,
    close: 45.15,
  },
  {
    date: '07/01/2011',
    volume: 77982800,
    close: 45.47,
  },
  {
    date: '10/01/2011',
    volume: 112140000,
    close: 46.33,
  },
  {
    date: '11/01/2011',
    volume: 111027000,
    close: 46.22,
  },
  {
    date: '12/01/2011',
    volume: 75647600,
    close: 46.59,
  },
  {
    date: '13/01/2011',
    volume: 74195100,
    close: 46.76,
  },
  {
    date: '14/01/2011',
    volume: 77210000,
    close: 47.14,
  },
  {
    date: '18/01/2011',
    volume: 470249500,
    close: 46.08,
  },
  {
    date: '19/01/2011',
    volume: 283903200,
    close: 45.84,
  },
  {
    date: '20/01/2011',
    volume: 191197300,
    close: 45.01,
  },
  {
    date: '21/01/2011',
    volume: 188600300,
    close: 44.2,
  },
  {
    date: '24/01/2011',
    volume: 143670800,
    close: 45.65,
  },
  {
    date: '25/01/2011',
    volume: 136717000,
    close: 46.19,
  },
  {
    date: '26/01/2011',
    volume: 126718900,
    close: 46.52,
  },
  {
    date: '27/01/2011',
    volume: 71256500,
    close: 46.43,
  },
  {
    date: '28/01/2011',
    volume: 148014300,
    close: 45.47,
  },
  {
    date: '31/01/2011',
    volume: 94311700,
    close: 45.9,
  },
  {
    date: '01/02/2011',
    volume: 106658300,
    close: 46.68,
  },
  {
    date: '02/02/2011',
    volume: 64738800,
    close: 46.58,
  },
  {
    date: '03/02/2011',
    volume: 98449400,
    close: 46.46,
  },
  {
    date: '04/02/2011',
    volume: 80460100,
    close: 46.88,
  },
  {
    date: '07/02/2011',
    volume: 121255400,
    close: 47.6,
  },
  {
    date: '08/02/2011',
    volume: 95260200,
    close: 48.05,
  },
  {
    date: '09/02/2011',
    volume: 120686300,
    close: 48.45,
  },
  {
    date: '10/02/2011',
    volume: 232137500,
    close: 47.96,
  },
  {
    date: '11/02/2011',
    volume: 91893200,
    close: 48.28,
  },
  {
    date: '14/02/2011',
    volume: 77604100,
    close: 48.59,
  },
  {
    date: '15/02/2011',
    volume: 71043700,
    close: 48.69,
  },
  {
    date: '16/02/2011',
    volume: 120289400,
    close: 49.13,
  },
  {
    date: '17/02/2011',
    volume: 132645800,
    close: 48.47,
  },
  {
    date: '18/02/2011',
    volume: 204014300,
    close: 47.42,
  },
  {
    date: '22/02/2011',
    volume: 218138900,
    close: 45.81,
  },
  {
    date: '23/02/2011',
    volume: 167963600,
    close: 46.35,
  },
  {
    date: '24/02/2011',
    volume: 124975200,
    close: 46.39,
  },
  {
    date: '25/02/2011',
    volume: 95004700,
    close: 47.1,
  },
  {
    date: '28/02/2011',
    volume: 100768500,
    close: 47.78,
  },
  {
    date: '01/03/2011',
    volume: 114034200,
    close: 47.26,
  },
  {
    date: '02/03/2011',
    volume: 150647700,
    close: 47.64,
  },
  {
    date: '03/03/2011',
    volume: 125197100,
    close: 48.64,
  },
  {
    date: '04/03/2011',
    volume: 113316700,
    close: 48.7,
  },
  {
    date: '07/03/2011',
    volume: 136530800,
    close: 48.07,
  },
  {
    date: '08/03/2011',
    volume: 89079200,
    close: 48.13,
  },
  {
    date: '09/03/2011',
    volume: 113326500,
    close: 47.68,
  },
  {
    date: '10/03/2011',
    volume: 126884800,
    close: 46.9,
  },
  {
    date: '11/03/2011',
    volume: 117770100,
    close: 47.62,
  },
  {
    date: '14/03/2011',
    volume: 108989300,
    close: 47.83,
  },
  {
    date: '15/03/2011',
    volume: 180270300,
    close: 46.73,
  },
  {
    date: '16/03/2011',
    volume: 290502800,
    close: 44.64,
  },
  {
    date: '17/03/2011',
    volume: 164855600,
    close: 45.27,
  },
  {
    date: '18/03/2011',
    volume: 188303500,
    close: 44.73,
  },
  {
    date: '21/03/2011',
    volume: 102350500,
    close: 45.9,
  },
  {
    date: '22/03/2011',
    volume: 81480700,
    close: 46.16,
  },
  {
    date: '23/03/2011',
    volume: 93249100,
    close: 45.89,
  },
  {
    date: '24/03/2011',
    volume: 101178000,
    close: 46.67,
  },
  {
    date: '25/03/2011',
    volume: 112227500,
    close: 47.56,
  },
  {
    date: '28/03/2011',
    volume: 77338800,
    close: 47.41,
  },
  {
    date: '29/03/2011',
    volume: 88225200,
    close: 47.48,
  },
  {
    date: '30/03/2011',
    volume: 82351500,
    close: 47.16,
  },
  {
    date: '31/03/2011',
    volume: 68504800,
    close: 47.15,
  },
  {
    date: '01/04/2011',
    volume: 104665400,
    close: 46.61,
  },
  {
    date: '04/04/2011',
    volume: 115021200,
    close: 46.16,
  },
  {
    date: '05/04/2011',
    volume: 120682800,
    close: 45.85,
  },
  {
    date: '06/04/2011',
    volume: 100634800,
    close: 45.73,
  },
  {
    date: '07/04/2011',
    volume: 93361800,
    close: 45.74,
  },
  {
    date: '08/04/2011',
    volume: 94383800,
    close: 45.33,
  },
  {
    date: '11/04/2011',
    volume: 99736700,
    close: 44.75,
  },
  {
    date: '12/04/2011',
    volume: 106409800,
    close: 44.97,
  },
  {
    date: '13/04/2011',
    volume: 86555000,
    close: 45.47,
  },
  {
    date: '14/04/2011',
    volume: 75450200,
    close: 44.97,
  },
  {
    date: '15/04/2011',
    volume: 113401400,
    close: 44.3,
  },
  {
    date: '18/04/2011',
    volume: 152474700,
    close: 44.89,
  },
  {
    date: '19/04/2011',
    volume: 104844600,
    close: 45.71,
  },
  {
    date: '20/04/2011',
    volume: 175166600,
    close: 46.32,
  },
  {
    date: '21/04/2011',
    volume: 188452600,
    close: 47.44,
  },
  {
    date: '25/04/2011',
    volume: 66636500,
    close: 47.76,
  },
  {
    date: '26/04/2011',
    volume: 84700000,
    close: 47.41,
  },
  {
    date: '27/04/2011',
    volume: 89053300,
    close: 47.37,
  },
  {
    date: '28/04/2011',
    volume: 90239800,
    close: 46.91,
  },
  {
    date: '29/04/2011',
    volume: 251586300,
    close: 47.37,
  },
  {
    date: '02/05/2011',
    volume: 110678400,
    close: 46.85,
  },
  {
    date: '03/05/2011',
    volume: 78337000,
    close: 47.11,
  },
  {
    date: '04/05/2011',
    volume: 97312600,
    close: 47.29,
  },
  {
    date: '05/05/2011',
    volume: 83992300,
    close: 46.91,
  },
  {
    date: '06/05/2011',
    volume: 70033600,
    close: 46.9,
  },
  {
    date: '09/05/2011',
    volume: 51186800,
    close: 47.02,
  },
  {
    date: '10/05/2011',
    volume: 70522900,
    close: 47.27,
  },
  {
    date: '11/05/2011',
    volume: 84000000,
    close: 46.97,
  },
  {
    date: '12/05/2011',
    volume: 80500000,
    close: 46.88,
  },
  {
    date: '13/05/2011',
    volume: 81529000,
    close: 46.06,
  },
  {
    date: '16/05/2011',
    volume: 112443800,
    close: 45.09,
  },
  {
    date: '17/05/2011',
    volume: 113083600,
    close: 45.47,
  },
  {
    date: '18/05/2011',
    volume: 83694100,
    close: 45.98,
  },
  {
    date: '19/05/2011',
    volume: 65292500,
    close: 46.07,
  },
  {
    date: '20/05/2011',
    volume: 84492100,
    close: 45.35,
  },
  {
    date: '23/05/2011',
    volume: 95900000,
    close: 45.24,
  },
  {
    date: '24/05/2011',
    volume: 80481800,
    close: 44.94,
  },
  {
    date: '25/05/2011',
    volume: 73556000,
    close: 45.56,
  },
  {
    date: '26/05/2011',
    volume: 55640200,
    close: 45.32,
  },
  {
    date: '27/05/2011',
    volume: 50899800,
    close: 45.65,
  },
  {
    date: '31/05/2011',
    volume: 104438600,
    close: 47.06,
  },
  {
    date: '01/06/2011',
    volume: 138670700,
    close: 46.74,
  },
  {
    date: '02/06/2011',
    volume: 84695800,
    close: 46.82,
  },
  {
    date: '03/06/2011',
    volume: 78312500,
    close: 46.46,
  },
  {
    date: '06/06/2011',
    volume: 115485300,
    close: 45.73,
  },
  {
    date: '07/06/2011',
    volume: 132446300,
    close: 44.92,
  },
  {
    date: '08/06/2011',
    volume: 83430900,
    close: 44.95,
  },
  {
    date: '09/06/2011',
    volume: 68772200,
    close: 44.84,
  },
  {
    date: '10/06/2011',
    volume: 108488800,
    close: 44.09,
  },
  {
    date: '13/06/2011',
    volume: 82368300,
    close: 44.18,
  },
  {
    date: '14/06/2011',
    volume: 83642300,
    close: 44.97,
  },
  {
    date: '15/06/2011',
    volume: 99799000,
    close: 44.2,
  },
  {
    date: '16/06/2011',
    volume: 127647800,
    close: 43.99,
  },
  {
    date: '17/06/2011',
    volume: 153755000,
    close: 43.33,
  },
  {
    date: '20/06/2011',
    volume: 160161400,
    close: 42.66,
  },
  {
    date: '21/06/2011',
    volume: 123345600,
    close: 44.01,
  },
  {
    date: '22/06/2011',
    volume: 97645800,
    close: 43.64,
  },
  {
    date: '23/06/2011',
    volume: 139939800,
    close: 44.81,
  },
  {
    date: '24/06/2011',
    volume: 109951800,
    close: 44.15,
  },
  {
    date: '27/06/2011',
    volume: 84953400,
    close: 44.92,
  },
  {
    date: '28/06/2011',
    volume: 73574900,
    close: 45.35,
  },
  {
    date: '29/06/2011',
    volume: 88136300,
    close: 45.19,
  },
  {
    date: '30/06/2011',
    volume: 80738700,
    close: 45.41,
  },
  {
    date: '01/07/2011',
    volume: 108828300,
    close: 46.44,
  },
  {
    date: '05/07/2011',
    volume: 88763500,
    close: 47.27,
  },
  {
    date: '06/07/2011',
    volume: 111156500,
    close: 47.59,
  },
  {
    date: '07/07/2011',
    volume: 99915900,
    close: 48.32,
  },
  {
    date: '08/07/2011',
    volume: 122408300,
    close: 48.66,
  },
  {
    date: '11/07/2011',
    volume: 110668600,
    close: 47.89,
  },
  {
    date: '12/07/2011',
    volume: 112902300,
    close: 47.86,
  },
  {
    date: '13/07/2011',
    volume: 97909700,
    close: 48.43,
  },
  {
    date: '14/07/2011',
    volume: 107633400,
    close: 48.4,
  },
  {
    date: '15/07/2011',
    volume: 121116800,
    close: 49.37,
  },
  {
    date: '18/07/2011',
    volume: 143163300,
    close: 50.57,
  },
  {
    date: '19/07/2011',
    volume: 204786400,
    close: 50.98,
  },
  {
    date: '20/07/2011',
    volume: 235335100,
    close: 52.34,
  },
  {
    date: '21/07/2011',
    volume: 131633600,
    close: 52.39,
  },
  {
    date: '22/07/2011',
    volume: 129182200,
    close: 53.21,
  },
  {
    date: '25/07/2011',
    volume: 147451500,
    close: 53.91,
  },
  {
    date: '26/07/2011',
    volume: 119145600,
    close: 54.57,
  },
  {
    date: '27/07/2011',
    volume: 164831100,
    close: 53.11,
  },
  {
    date: '28/07/2011',
    volume: 148508500,
    close: 53.01,
  },
  {
    date: '29/07/2011',
    volume: 158146100,
    close: 52.83,
  },
  {
    date: '01/08/2011',
    volume: 153209000,
    close: 53.67,
  },
  {
    date: '02/08/2011',
    volume: 159884900,
    close: 52.61,
  },
  {
    date: '03/08/2011',
    volume: 183127000,
    close: 53.11,
  },
  {
    date: '04/08/2011',
    volume: 217851900,
    close: 51.05,
  },
  {
    date: '05/08/2011',
    volume: 301147700,
    close: 50.54,
  },
  {
    date: '08/08/2011',
    volume: 285958400,
    close: 47.78,
  },
  {
    date: '09/08/2011',
    volume: 270645900,
    close: 50.6,
  },
  {
    date: '10/08/2011',
    volume: 219664200,
    close: 49.2,
  },
  {
    date: '11/08/2011',
    volume: 185492300,
    close: 50.56,
  },
  {
    date: '12/08/2011',
    volume: 132244000,
    close: 51,
  },
  {
    date: '15/08/2011',
    volume: 115136000,
    close: 51.87,
  },
  {
    date: '16/08/2011',
    volume: 124687500,
    close: 51.47,
  },
  {
    date: '17/08/2011',
    volume: 110515300,
    close: 51.47,
  },
  {
    date: '18/08/2011',
    volume: 212858800,
    close: 49.52,
  },
  {
    date: '19/08/2011',
    volume: 193972100,
    close: 48.16,
  },
  {
    date: '22/08/2011',
    volume: 133828800,
    close: 48.22,
  },
  {
    date: '23/08/2011',
    volume: 164208800,
    close: 50.54,
  },
  {
    date: '24/08/2011',
    volume: 156566900,
    close: 50.89,
  },
  {
    date: '25/08/2011',
    volume: 217836500,
    close: 50.56,
  },
  {
    date: '26/08/2011',
    volume: 160369300,
    close: 51.89,
  },
  {
    date: '29/08/2011',
    volume: 101317300,
    close: 52.76,
  },
  {
    date: '30/08/2011',
    volume: 104480600,
    close: 52.76,
  },
  {
    date: '31/08/2011',
    volume: 130646600,
    close: 52.06,
  },
  {
    date: '01/09/2011',
    volume: 85931300,
    close: 51.55,
  },
  {
    date: '02/09/2011',
    volume: 109734800,
    close: 50.6,
  },
  {
    date: '06/09/2011',
    volume: 127424500,
    close: 51.37,
  },
  {
    date: '07/09/2011',
    volume: 87644200,
    close: 51.94,
  },
  {
    date: '08/09/2011',
    volume: 104039600,
    close: 51.97,
  },
  {
    date: '09/09/2011',
    volume: 141203300,
    close: 51.07,
  },
  {
    date: '12/09/2011',
    volume: 116958100,
    close: 51.4,
  },
  {
    date: '13/09/2011',
    volume: 110140100,
    close: 52.03,
  },
  {
    date: '14/09/2011',
    volume: 133681100,
    close: 52.67,
  },
  {
    date: '15/09/2011',
    volume: 104454700,
    close: 53.16,
  },
  {
    date: '16/09/2011',
    volume: 174628300,
    close: 54.18,
  },
  {
    date: '19/09/2011',
    volume: 205965200,
    close: 55.69,
  },
  {
    date: '20/09/2011',
    volume: 193938500,
    close: 55.93,
  },
  {
    date: '21/09/2011',
    volume: 151494000,
    close: 55.76,
  },
  {
    date: '22/09/2011',
    volume: 242120200,
    close: 54.36,
  },
  {
    date: '23/09/2011',
    volume: 136569300,
    close: 54.69,
  },
  {
    date: '26/09/2011',
    volume: 203219100,
    close: 54.54,
  },
  {
    date: '27/09/2011',
    volume: 158124400,
    close: 54.01,
  },
  {
    date: '28/09/2011',
    volume: 107409400,
    close: 53.71,
  },
  {
    date: '29/09/2011',
    volume: 162771700,
    close: 52.84,
  },
  {
    date: '30/09/2011',
    volume: 136910200,
    close: 51.59,
  },
  {
    date: '03/10/2011',
    volume: 167274800,
    close: 50.68,
  },
  {
    date: '04/10/2011',
    volume: 308419300,
    close: 50.39,
  },
  {
    date: '05/10/2011',
    volume: 196617400,
    close: 51.17,
  },
  {
    date: '06/10/2011',
    volume: 203145600,
    close: 51.05,
  },
  {
    date: '07/10/2011',
    volume: 133864500,
    close: 50.03,
  },
  {
    date: '10/10/2011',
    volume: 110628700,
    close: 52.6,
  },
  {
    date: '11/10/2011',
    volume: 151421900,
    close: 54.15,
  },
  {
    date: '12/10/2011',
    volume: 155571500,
    close: 54.41,
  },
  {
    date: '13/10/2011',
    volume: 106546300,
    close: 55.25,
  },
  {
    date: '14/10/2011',
    volume: 143341800,
    close: 57.09,
  },
  {
    date: '17/10/2011',
    volume: 171511200,
    close: 56.82,
  },
  {
    date: '18/10/2011',
    volume: 220400600,
    close: 57.12,
  },
  {
    date: '19/10/2011',
    volume: 276014900,
    close: 53.93,
  },
  {
    date: '20/10/2011',
    volume: 137317600,
    close: 53.48,
  },
  {
    date: '21/10/2011',
    volume: 155311100,
    close: 53.15,
  },
  {
    date: '24/10/2011',
    volume: 125534500,
    close: 54.89,
  },
  {
    date: '25/10/2011',
    volume: 107606800,
    close: 53.81,
  },
  {
    date: '26/10/2011',
    volume: 114076200,
    close: 54.19,
  },
  {
    date: '27/10/2011',
    volume: 123666200,
    close: 54.75,
  },
  {
    date: '28/10/2011',
    volume: 80710700,
    close: 54.78,
  },
  {
    date: '31/10/2011',
    volume: 96375300,
    close: 54.76,
  },
  {
    date: '01/11/2011',
    volume: 132947500,
    close: 53.64,
  },
  {
    date: '02/11/2011',
    volume: 81837700,
    close: 53.76,
  },
  {
    date: '03/11/2011',
    volume: 110346600,
    close: 54.53,
  },
  {
    date: '04/11/2011',
    volume: 75557300,
    close: 54.15,
  },
  {
    date: '07/11/2011',
    volume: 67568900,
    close: 54.08,
  },
  {
    date: '08/11/2011',
    volume: 100110500,
    close: 54.96,
  },
  {
    date: '09/11/2011',
    volume: 139671000,
    close: 53.47,
  },
  {
    date: '10/11/2011',
    volume: 186188100,
    close: 52.11,
  },
  {
    date: '11/11/2011',
    volume: 163446500,
    close: 52.03,
  },
  {
    date: '14/11/2011',
    volume: 108226300,
    close: 51.31,
  },
  {
    date: '15/11/2011',
    volume: 107702700,
    close: 52.6,
  },
  {
    date: '16/11/2011',
    volume: 87302600,
    close: 52.05,
  },
  {
    date: '17/11/2011',
    volume: 119975100,
    close: 51.06,
  },
  {
    date: '18/11/2011',
    volume: 92984500,
    close: 50.72,
  },
  {
    date: '21/11/2011',
    volume: 111995100,
    close: 49.92,
  },
  {
    date: '22/11/2011',
    volume: 102255300,
    close: 50.94,
  },
  {
    date: '23/11/2011',
    volume: 107067800,
    close: 49.65,
  },
  {
    date: '25/11/2011',
    volume: 63690200,
    close: 49.18,
  },
  {
    date: '28/11/2011',
    volume: 86603300,
    close: 50.88,
  },
  {
    date: '29/11/2011',
    volume: 93963800,
    close: 50.49,
  },
  {
    date: '30/11/2011',
    volume: 101484600,
    close: 51.71,
  },
  {
    date: '01/12/2011',
    volume: 96795300,
    close: 52.48,
  },
  {
    date: '02/12/2011',
    volume: 94763900,
    close: 52.72,
  },
  {
    date: '05/12/2011',
    volume: 89302500,
    close: 53.17,
  },
  {
    date: '06/12/2011',
    volume: 70899500,
    close: 52.89,
  },
  {
    date: '07/12/2011',
    volume: 76186600,
    close: 52.64,
  },
  {
    date: '08/12/2011',
    volume: 94089100,
    close: 52.85,
  },
  {
    date: '09/12/2011',
    volume: 74248300,
    close: 53.25,
  },
  {
    date: '12/12/2011',
    volume: 75266800,
    close: 53.01,
  },
  {
    date: '13/12/2011',
    volume: 84732200,
    close: 52.6,
  },
  {
    date: '14/12/2011',
    volume: 101721900,
    close: 51.43,
  },
  {
    date: '15/12/2011',
    volume: 64050000,
    close: 51.26,
  },
  {
    date: '16/12/2011',
    volume: 105369600,
    close: 51.55,
  },
  {
    date: '19/12/2011',
    volume: 58882600,
    close: 51.71,
  },
  {
    date: '20/12/2011',
    volume: 84303800,
    close: 53.57,
  },
  {
    date: '21/12/2011',
    volume: 65737000,
    close: 53.63,
  },
  {
    date: '22/12/2011',
    volume: 50589700,
    close: 53.92,
  },
  {
    date: '23/12/2011',
    volume: 67349800,
    close: 54.56,
  },
  {
    date: '27/12/2011',
    volume: 66269000,
    close: 55,
  },
  {
    date: '28/12/2011',
    volume: 57165500,
    close: 54.47,
  },
  {
    date: '29/12/2011',
    volume: 53994500,
    close: 54.81,
  },
  {
    date: '30/12/2011',
    volume: 44915500,
    close: 54.79,
  },
  {
    date: '03/01/2012',
    volume: 75555200,
    close: 55.63,
  },
  {
    date: '04/01/2012',
    volume: 65005500,
    close: 55.93,
  },
  {
    date: '05/01/2012',
    volume: 67817400,
    close: 56.55,
  },
  {
    date: '06/01/2012',
    volume: 79573200,
    close: 57.14,
  },
  {
    date: '09/01/2012',
    volume: 98506100,
    close: 57.05,
  },
  {
    date: '10/01/2012',
    volume: 64549100,
    close: 57.26,
  },
  {
    date: '11/01/2012',
    volume: 53771200,
    close: 57.16,
  },
  {
    date: '12/01/2012',
    volume: 53146800,
    close: 57.01,
  },
  {
    date: '13/01/2012',
    volume: 56505400,
    close: 56.79,
  },
  {
    date: '17/01/2012',
    volume: 60724300,
    close: 57.45,
  },
  {
    date: '18/01/2012',
    volume: 69197800,
    close: 58.05,
  },
  {
    date: '19/01/2012',
    volume: 65434600,
    close: 57.87,
  },
  {
    date: '20/01/2012',
    volume: 103493600,
    close: 56.86,
  },
  {
    date: '23/01/2012',
    volume: 76515600,
    close: 57.82,
  },
  {
    date: '24/01/2012',
    volume: 136909500,
    close: 56.87,
  },
  {
    date: '25/01/2012',
    volume: 239578500,
    close: 60.43,
  },
  {
    date: '26/01/2012',
    volume: 80996300,
    close: 60.15,
  },
  {
    date: '27/01/2012',
    volume: 74927300,
    close: 60.51,
  },
  {
    date: '30/01/2012',
    volume: 94835300,
    close: 61.28,
  },
  {
    date: '31/01/2012',
    volume: 97920900,
    close: 61.75,
  },
  {
    date: '01/02/2012',
    volume: 67511500,
    close: 61.71,
  },
  {
    date: '02/02/2012',
    volume: 46699100,
    close: 61.57,
  },
  {
    date: '03/02/2012',
    volume: 71649900,
    close: 62.19,
  },
  {
    date: '06/02/2012',
    volume: 62353200,
    close: 62.77,
  },
  {
    date: '07/02/2012',
    volume: 79055900,
    close: 63.42,
  },
  {
    date: '08/02/2012',
    volume: 101972500,
    close: 64.49,
  },
  {
    date: '09/02/2012',
    volume: 221053700,
    close: 66.72,
  },
  {
    date: '10/02/2012',
    volume: 157825500,
    close: 66.75,
  },
  {
    date: '13/02/2012',
    volume: 129304000,
    close: 67.99,
  },
  {
    date: '14/02/2012',
    volume: 115099600,
    close: 68.92,
  },
  {
    date: '15/02/2012',
    volume: 376530000,
    close: 67.33,
  },
  {
    date: '16/02/2012',
    volume: 236138000,
    close: 67.94,
  },
  {
    date: '17/02/2012',
    volume: 133951300,
    close: 67.93,
  },
  {
    date: '21/02/2012',
    volume: 151398800,
    close: 69.65,
  },
  {
    date: '22/02/2012',
    volume: 120825600,
    close: 69.41,
  },
  {
    date: '23/02/2012',
    volume: 142006900,
    close: 69.86,
  },
  {
    date: '24/02/2012',
    volume: 103768000,
    close: 70.67,
  },
  {
    date: '27/02/2012',
    volume: 136895500,
    close: 71.13,
  },
  {
    date: '28/02/2012',
    volume: 150096800,
    close: 72.43,
  },
  {
    date: '29/02/2012',
    volume: 238002800,
    close: 73.38,
  },
  {
    date: '01/03/2012',
    volume: 170817500,
    close: 73.66,
  },
  {
    date: '02/03/2012',
    volume: 107928100,
    close: 73.75,
  },
  {
    date: '05/03/2012',
    volume: 202281100,
    close: 72.13,
  },
  {
    date: '06/03/2012',
    volume: 202559700,
    close: 71.74,
  },
  {
    date: '07/03/2012',
    volume: 199630200,
    close: 71.79,
  },
  {
    date: '08/03/2012',
    volume: 129114300,
    close: 73.32,
  },
  {
    date: '09/03/2012',
    volume: 104729800,
    close: 73.75,
  },
  {
    date: '12/03/2012',
    volume: 101820600,
    close: 74.68,
  },
  {
    date: '13/03/2012',
    volume: 172713800,
    close: 76.85,
  },
  {
    date: '14/03/2012',
    volume: 354711000,
    close: 79.76,
  },
  {
    date: '15/03/2012',
    volume: 289929500,
    close: 79.22,
  },
  {
    date: '16/03/2012',
    volume: 206371900,
    close: 79.22,
  },
  {
    date: '19/03/2012',
    volume: 225309000,
    close: 81.32,
  },
  {
    date: '20/03/2012',
    volume: 204165500,
    close: 81.98,
  },
  {
    date: '21/03/2012',
    volume: 161010500,
    close: 81.51,
  },
  {
    date: '22/03/2012',
    volume: 155967700,
    close: 81.08,
  },
  {
    date: '23/03/2012',
    volume: 107622200,
    close: 80.64,
  },
  {
    date: '26/03/2012',
    volume: 148935500,
    close: 82.11,
  },
  {
    date: '27/03/2012',
    volume: 151782400,
    close: 83.13,
  },
  {
    date: '28/03/2012',
    volume: 163865100,
    close: 83.55,
  },
  {
    date: '29/03/2012',
    volume: 152059600,
    close: 82.5,
  },
  {
    date: '30/03/2012',
    volume: 182759500,
    close: 81.11,
  },
  {
    date: '02/04/2012',
    volume: 149587900,
    close: 83.69,
  },
  {
    date: '03/04/2012',
    volume: 208639900,
    close: 85.14,
  },
  {
    date: '04/04/2012',
    volume: 143245200,
    close: 84.46,
  },
  {
    date: '05/04/2012',
    volume: 160324500,
    close: 85.73,
  },
  {
    date: '09/04/2012',
    volume: 149384200,
    close: 86.07,
  },
  {
    date: '10/04/2012',
    volume: 222431300,
    close: 85.02,
  },
  {
    date: '11/04/2012',
    volume: 174153700,
    close: 84.71,
  },
  {
    date: '12/04/2012',
    volume: 153584200,
    close: 84.25,
  },
  {
    date: '13/04/2012',
    volume: 214911200,
    close: 81.88,
  },
  {
    date: '16/04/2012',
    volume: 262696700,
    close: 78.48,
  },
  {
    date: '17/04/2012',
    volume: 256382000,
    close: 82.48,
  },
  {
    date: '18/04/2012',
    volume: 238632800,
    close: 82.3,
  },
  {
    date: '19/04/2012',
    volume: 208679800,
    close: 79.47,
  },
  {
    date: '20/04/2012',
    volume: 257746300,
    close: 77.51,
  },
  {
    date: '23/04/2012',
    volume: 241632300,
    close: 77.34,
  },
  {
    date: '24/04/2012',
    volume: 269037300,
    close: 75.8,
  },
  {
    date: '25/04/2012',
    volume: 226444400,
    close: 82.52,
  },
  {
    date: '26/04/2012',
    volume: 134017100,
    close: 82.21,
  },
  {
    date: '27/04/2012',
    volume: 101680600,
    close: 81.58,
  },
  {
    date: '30/04/2012',
    volume: 126536200,
    close: 79,
  },
  {
    date: '01/05/2012',
    volume: 152749800,
    close: 78.75,
  },
  {
    date: '02/05/2012',
    volume: 106847300,
    close: 79.27,
  },
  {
    date: '03/05/2012',
    volume: 97637400,
    close: 78.71,
  },
  {
    date: '04/05/2012',
    volume: 132498100,
    close: 76.47,
  },
  {
    date: '07/05/2012',
    volume: 115029600,
    close: 77.04,
  },
  {
    date: '08/05/2012',
    volume: 124313000,
    close: 76.86,
  },
  {
    date: '09/05/2012',
    volume: 120176000,
    close: 77,
  },
  {
    date: '10/05/2012',
    volume: 83300000,
    close: 77.18,
  },
  {
    date: '11/05/2012',
    volume: 99886500,
    close: 76.67,
  },
  {
    date: '14/05/2012',
    volume: 88156600,
    close: 75.52,
  },
  {
    date: '15/05/2012',
    volume: 119084000,
    close: 74.83,
  },
  {
    date: '16/05/2012',
    volume: 140224000,
    close: 73.88,
  },
  {
    date: '17/05/2012',
    volume: 179305000,
    close: 71.72,
  },
  {
    date: '18/05/2012',
    volume: 183073100,
    close: 71.75,
  },
  {
    date: '21/05/2012',
    volume: 157776500,
    close: 75.93,
  },
  {
    date: '22/05/2012',
    volume: 173717600,
    close: 75.35,
  },
  {
    date: '23/05/2012',
    volume: 146224400,
    close: 77.19,
  },
  {
    date: '24/05/2012',
    volume: 124057500,
    close: 76.48,
  },
  {
    date: '25/05/2012',
    volume: 82126800,
    close: 76.07,
  },
  {
    date: '29/05/2012',
    volume: 95127200,
    close: 77.42,
  },
  {
    date: '30/05/2012',
    volume: 132357400,
    close: 78.35,
  },
  {
    date: '31/05/2012',
    volume: 122918600,
    close: 78.16,
  },
  {
    date: '01/06/2012',
    volume: 130246900,
    close: 75.89,
  },
  {
    date: '04/06/2012',
    volume: 139248900,
    close: 76.34,
  },
  {
    date: '05/06/2012',
    volume: 97053600,
    close: 76.14,
  },
  {
    date: '06/06/2012',
    volume: 100363900,
    close: 77.31,
  },
  {
    date: '07/06/2012',
    volume: 94941700,
    close: 77.34,
  },
  {
    date: '08/06/2012',
    volume: 86879100,
    close: 78.51,
  },
  {
    date: '11/06/2012',
    volume: 147816200,
    close: 77.27,
  },
  {
    date: '12/06/2012',
    volume: 108845100,
    close: 77.94,
  },
  {
    date: '13/06/2012',
    volume: 73395000,
    close: 77.4,
  },
  {
    date: '14/06/2012',
    volume: 86393300,
    close: 77.32,
  },
  {
    date: '15/06/2012',
    volume: 83813800,
    close: 77.67,
  },
  {
    date: '18/06/2012',
    volume: 110103000,
    close: 79.25,
  },
  {
    date: '19/06/2012',
    volume: 90351100,
    close: 79.47,
  },
  {
    date: '20/06/2012',
    volume: 89735800,
    close: 79.24,
  },
  {
    date: '21/06/2012',
    volume: 81587800,
    close: 78.15,
  },
  {
    date: '22/06/2012',
    volume: 71117900,
    close: 78.75,
  },
  {
    date: '25/06/2012',
    volume: 76095600,
    close: 77.22,
  },
  {
    date: '26/06/2012',
    volume: 69134100,
    close: 77.39,
  },
  {
    date: '27/06/2012',
    volume: 50749300,
    close: 77.72,
  },
  {
    date: '28/06/2012',
    volume: 70709100,
    close: 76.98,
  },
  {
    date: '29/06/2012',
    volume: 105375200,
    close: 79.01,
  },
  {
    date: '02/07/2012',
    volume: 100023000,
    close: 80.16,
  },
  {
    date: '03/07/2012',
    volume: 60428200,
    close: 81.09,
  },
  {
    date: '05/07/2012',
    volume: 121095800,
    close: 82.51,
  },
  {
    date: '06/07/2012',
    volume: 104732600,
    close: 81.97,
  },
  {
    date: '09/07/2012',
    volume: 94851400,
    close: 83.05,
  },
  {
    date: '10/07/2012',
    volume: 127989400,
    close: 82.28,
  },
  {
    date: '11/07/2012',
    volume: 117330500,
    close: 81.77,
  },
  {
    date: '12/07/2012',
    volume: 107010400,
    close: 81.02,
  },
  {
    date: '13/07/2012',
    volume: 77856800,
    close: 81.84,
  },
  {
    date: '16/07/2012',
    volume: 75315100,
    close: 82.1,
  },
  {
    date: '17/07/2012',
    volume: 73406200,
    close: 82.11,
  },
  {
    date: '18/07/2012',
    volume: 63175000,
    close: 82.02,
  },
  {
    date: '19/07/2012',
    volume: 109215400,
    close: 83.11,
  },
  {
    date: '20/07/2012',
    volume: 99367800,
    close: 81.75,
  },
  {
    date: '23/07/2012',
    volume: 121993900,
    close: 81.69,
  },
  {
    date: '24/07/2012',
    volume: 141283100,
    close: 81.29,
  },
  {
    date: '25/07/2012',
    volume: 219328200,
    close: 77.78,
  },
  {
    date: '26/07/2012',
    volume: 101658200,
    close: 77.77,
  },
  {
    date: '27/07/2012',
    volume: 100984100,
    close: 79.16,
  },
  {
    date: '30/07/2012',
    volume: 94785600,
    close: 80.5,
  },
  {
    date: '31/07/2012',
    volume: 115581900,
    close: 82.63,
  },
  {
    date: '01/08/2012',
    volume: 96125400,
    close: 82.09,
  },
  {
    date: '02/08/2012',
    volume: 83039600,
    close: 82.22,
  },
  {
    date: '03/08/2012',
    volume: 86230200,
    close: 83.29,
  },
  {
    date: '06/08/2012',
    volume: 75525800,
    close: 84.22,
  },
  {
    date: '07/08/2012',
    volume: 72611700,
    close: 84,
  },
  {
    date: '08/08/2012',
    volume: 61176500,
    close: 83.86,
  },
  {
    date: '09/08/2012',
    volume: 55410600,
    close: 84.33,
  },
  {
    date: '10/08/2012',
    volume: 48734700,
    close: 84.47,
  },
  {
    date: '13/08/2012',
    volume: 69708100,
    close: 85.59,
  },
  {
    date: '14/08/2012',
    volume: 85042300,
    close: 85.82,
  },
  {
    date: '15/08/2012',
    volume: 64335600,
    close: 85.71,
  },
  {
    date: '16/08/2012',
    volume: 63633500,
    close: 86.46,
  },
  {
    date: '17/08/2012',
    volume: 110690300,
    close: 88.05,
  },
  {
    date: '20/08/2012',
    volume: 153346200,
    close: 90.37,
  },
  {
    date: '21/08/2012',
    volume: 203179900,
    close: 89.13,
  },
  {
    date: '22/08/2012',
    volume: 141330700,
    close: 90.88,
  },
  {
    date: '23/08/2012',
    volume: 105032200,
    close: 90.03,
  },
  {
    date: '24/08/2012',
    volume: 109335100,
    close: 90.11,
  },
  {
    date: '27/08/2012',
    volume: 106752100,
    close: 91.8,
  },
  {
    date: '28/08/2012',
    volume: 66854200,
    close: 91.68,
  },
  {
    date: '29/08/2012',
    volume: 50701700,
    close: 91.5,
  },
  {
    date: '30/08/2012',
    volume: 75674900,
    close: 90.2,
  },
  {
    date: '31/08/2012',
    volume: 84580300,
    close: 90.38,
  },
  {
    date: '04/09/2012',
    volume: 91973000,
    close: 91.7,
  },
  {
    date: '05/09/2012',
    volume: 84093800,
    close: 91.06,
  },
  {
    date: '06/09/2012',
    volume: 97799100,
    close: 91.88,
  },
  {
    date: '07/09/2012',
    volume: 82416600,
    close: 92.45,
  },
  {
    date: '10/09/2012',
    volume: 121999500,
    close: 90.04,
  },
  {
    date: '11/09/2012',
    volume: 125995800,
    close: 89.75,
  },
  {
    date: '12/09/2012',
    volume: 178058300,
    close: 91,
  },
  {
    date: '13/09/2012',
    volume: 149590000,
    close: 92.79,
  },
  {
    date: '14/09/2012',
    volume: 150118500,
    close: 93.92,
  },
  {
    date: '17/09/2012',
    volume: 99507800,
    close: 95.07,
  },
  {
    date: '18/09/2012',
    volume: 93375800,
    close: 95.36,
  },
  {
    date: '19/09/2012',
    volume: 81718700,
    close: 95.39,
  },
  {
    date: '20/09/2012',
    volume: 84142100,
    close: 94.93,
  },
  {
    date: '21/09/2012',
    volume: 142897300,
    close: 95.12,
  },
  {
    date: '24/09/2012',
    volume: 159941600,
    close: 93.85,
  },
  {
    date: '25/09/2012',
    volume: 129697400,
    close: 91.51,
  },
  {
    date: '26/09/2012',
    volume: 144125800,
    close: 90.37,
  },
  {
    date: '27/09/2012',
    volume: 148522500,
    close: 92.57,
  },
  {
    date: '28/09/2012',
    volume: 133777700,
    close: 90.63,
  },
  {
    date: '01/10/2012',
    volume: 135898700,
    close: 89.59,
  },
  {
    date: '02/10/2012',
    volume: 156998100,
    close: 89.85,
  },
  {
    date: '03/10/2012',
    volume: 106070300,
    close: 91.23,
  },
  {
    date: '04/10/2012',
    volume: 92681400,
    close: 90.59,
  },
  {
    date: '05/10/2012',
    volume: 148501500,
    close: 88.66,
  },
  {
    date: '08/10/2012',
    volume: 159498500,
    close: 86.7,
  },
  {
    date: '09/10/2012',
    volume: 209649300,
    close: 86.39,
  },
  {
    date: '10/10/2012',
    volume: 127589000,
    close: 87.08,
  },
  {
    date: '11/10/2012',
    volume: 136520300,
    close: 85.34,
  },
  {
    date: '12/10/2012',
    volume: 115003700,
    close: 85.55,
  },
  {
    date: '15/10/2012',
    volume: 108125500,
    close: 86.24,
  },
  {
    date: '16/10/2012',
    volume: 137442900,
    close: 88.28,
  },
  {
    date: '17/10/2012',
    volume: 97259400,
    close: 87.58,
  },
  {
    date: '18/10/2012',
    volume: 119156100,
    close: 85.95,
  },
  {
    date: '19/10/2012',
    volume: 186021500,
    close: 82.86,
  },
  {
    date: '22/10/2012',
    volume: 136682700,
    close: 86.14,
  },
  {
    date: '23/10/2012',
    volume: 176786400,
    close: 83.33,
  },
  {
    date: '24/10/2012',
    volume: 139631800,
    close: 83.8,
  },
  {
    date: '25/10/2012',
    volume: 164081400,
    close: 82.81,
  },
  {
    date: '26/10/2012',
    volume: 254608200,
    close: 82.06,
  },
  {
    date: '31/10/2012',
    volume: 127500800,
    close: 80.88,
  },
  {
    date: '01/11/2012',
    volume: 90324500,
    close: 81.05,
  },
  {
    date: '02/11/2012',
    volume: 149843400,
    close: 78.37,
  },
  {
    date: '05/11/2012',
    volume: 132283900,
    close: 79.43,
  },
  {
    date: '06/11/2012',
    volume: 93729300,
    close: 79.19,
  },
  {
    date: '07/11/2012',
    volume: 198412200,
    close: 76.16,
  },
  {
    date: '08/11/2012',
    volume: 264036500,
    close: 73.39,
  },
  {
    date: '09/11/2012',
    volume: 232478400,
    close: 74.66,
  },
  {
    date: '12/11/2012',
    volume: 128950500,
    close: 74.09,
  },
  {
    date: '13/11/2012',
    volume: 133237300,
    close: 74.1,
  },
  {
    date: '14/11/2012',
    volume: 119292600,
    close: 73.28,
  },
  {
    date: '15/11/2012',
    volume: 197477700,
    close: 71.74,
  },
  {
    date: '16/11/2012',
    volume: 316723400,
    close: 72.02,
  },
  {
    date: '19/11/2012',
    volume: 205829400,
    close: 77.21,
  },
  {
    date: '20/11/2012',
    volume: 160688500,
    close: 76.56,
  },
  {
    date: '21/11/2012',
    volume: 93250500,
    close: 76.66,
  },
  {
    date: '23/11/2012',
    volume: 68206600,
    close: 78,
  },
  {
    date: '26/11/2012',
    volume: 157644900,
    close: 80.46,
  },
  {
    date: '27/11/2012',
    volume: 133332500,
    close: 79.81,
  },
  {
    date: '28/11/2012',
    volume: 130216100,
    close: 79.56,
  },
  {
    date: '29/11/2012',
    volume: 128674700,
    close: 80.44,
  },
  {
    date: '30/11/2012',
    volume: 97829900,
    close: 79.88,
  },
  {
    date: '03/12/2012',
    volume: 91070000,
    close: 80.01,
  },
  {
    date: '04/12/2012',
    volume: 139267100,
    close: 78.59,
  },
  {
    date: '05/12/2012',
    volume: 261159500,
    close: 73.54,
  },
  {
    date: '06/12/2012',
    volume: 294303100,
    close: 74.69,
  },
  {
    date: '07/12/2012',
    volume: 196760200,
    close: 72.78,
  },
  {
    date: '10/12/2012',
    volume: 157621100,
    close: 72.31,
  },
  {
    date: '11/12/2012',
    volume: 148086400,
    close: 73.89,
  },
  {
    date: '12/12/2012',
    volume: 121786000,
    close: 73.56,
  },
  {
    date: '13/12/2012',
    volume: 156314900,
    close: 72.29,
  },
  {
    date: '14/12/2012',
    volume: 252394800,
    close: 69.58,
  },
  {
    date: '17/12/2012',
    volume: 189401800,
    close: 70.81,
  },
  {
    date: '18/12/2012',
    volume: 156421300,
    close: 72.87,
  },
  {
    date: '19/12/2012',
    volume: 112342300,
    close: 71.83,
  },
  {
    date: '20/12/2012',
    volume: 120422400,
    close: 71.21,
  },
  {
    date: '21/12/2012',
    volume: 149067100,
    close: 70.88,
  },
  {
    date: '24/12/2012',
    volume: 43938300,
    close: 70.99,
  },
  {
    date: '26/12/2012',
    volume: 75609100,
    close: 70.02,
  },
  {
    date: '27/12/2012',
    volume: 113780100,
    close: 70.3,
  },
  {
    date: '28/12/2012',
    volume: 88569600,
    close: 69.55,
  },
  {
    date: '31/12/2012',
    volume: 164873100,
    close: 72.63,
  },
  {
    date: '02/01/2013',
    volume: 140129500,
    close: 74.93,
  },
  {
    date: '03/01/2013',
    volume: 88241300,
    close: 73.99,
  },
  {
    date: '04/01/2013',
    volume: 148583400,
    close: 71.93,
  },
  {
    date: '07/01/2013',
    volume: 121039100,
    close: 71.5,
  },
  {
    date: '08/01/2013',
    volume: 114676800,
    close: 71.7,
  },
  {
    date: '09/01/2013',
    volume: 101901100,
    close: 70.58,
  },
  {
    date: '10/01/2013',
    volume: 150286500,
    close: 71.45,
  },
  {
    date: '11/01/2013',
    volume: 87626700,
    close: 71.01,
  },
  {
    date: '14/01/2013',
    volume: 183551900,
    close: 68.48,
  },
  {
    date: '15/01/2013',
    volume: 219193100,
    close: 66.32,
  },
  {
    date: '16/01/2013',
    volume: 172701200,
    close: 69.07,
  },
  {
    date: '17/01/2013',
    volume: 113419600,
    close: 68.61,
  },
  {
    date: '18/01/2013',
    volume: 118230700,
    close: 68.24,
  },
  {
    date: '22/01/2013',
    volume: 115386600,
    close: 68.89,
  },
  {
    date: '23/01/2013',
    volume: 215377400,
    close: 70.15,
  },
  {
    date: '24/01/2013',
    volume: 365213100,
    close: 61.49,
  },
  {
    date: '25/01/2013',
    volume: 302006600,
    close: 60.04,
  },
  {
    date: '28/01/2013',
    volume: 196379400,
    close: 61.39,
  },
  {
    date: '29/01/2013',
    volume: 142789500,
    close: 62.55,
  },
  {
    date: '30/01/2013',
    volume: 104288800,
    close: 62.35,
  },
  {
    date: '31/01/2013',
    volume: 79833600,
    close: 62.17,
  },
  {
    date: '01/02/2013',
    volume: 134871100,
    close: 61.91,
  },
  {
    date: '04/02/2013',
    volume: 119279300,
    close: 60.37,
  },
  {
    date: '05/02/2013',
    volume: 143336900,
    close: 62.49,
  },
  {
    date: '06/02/2013',
    volume: 148426600,
    close: 62.42,
  },
  {
    date: '07/02/2013',
    volume: 176145200,
    close: 64.28,
  },
  {
    date: '08/02/2013',
    volume: 158289600,
    close: 65.21,
  },
  {
    date: '11/02/2013',
    volume: 129372600,
    close: 65.88,
  },
  {
    date: '12/02/2013',
    volume: 152263300,
    close: 64.23,
  },
  {
    date: '13/02/2013',
    volume: 118801900,
    close: 64.11,
  },
  {
    date: '14/02/2013',
    volume: 88818800,
    close: 64.05,
  },
  {
    date: '15/02/2013',
    volume: 97936300,
    close: 63.17,
  },
  {
    date: '19/02/2013',
    volume: 108945900,
    close: 63.15,
  },
  {
    date: '20/02/2013',
    volume: 119075600,
    close: 61.62,
  },
  {
    date: '21/02/2013',
    volume: 111795600,
    close: 61.23,
  },
  {
    date: '22/02/2013',
    volume: 82663700,
    close: 61.89,
  },
  {
    date: '25/02/2013',
    volume: 93144800,
    close: 60.79,
  },
  {
    date: '26/02/2013',
    volume: 125374900,
    close: 61.63,
  },
  {
    date: '27/02/2013',
    volume: 146837600,
    close: 61.03,
  },
  {
    date: '28/02/2013',
    volume: 80628800,
    close: 60.6,
  },
  {
    date: '01/03/2013',
    volume: 138112100,
    close: 59.09,
  },
  {
    date: '04/03/2013',
    volume: 145688900,
    close: 57.66,
  },
  {
    date: '05/03/2013',
    volume: 159608400,
    close: 59.19,
  },
  {
    date: '06/03/2013',
    volume: 115062500,
    close: 58.43,
  },
  {
    date: '07/03/2013',
    volume: 117118400,
    close: 59.11,
  },
  {
    date: '08/03/2013',
    volume: 97870500,
    close: 59.27,
  },
  {
    date: '11/03/2013',
    volume: 118559000,
    close: 60.11,
  },
  {
    date: '12/03/2013',
    volume: 116477900,
    close: 58.81,
  },
  {
    date: '13/03/2013',
    volume: 101387300,
    close: 58.8,
  },
  {
    date: '14/03/2013',
    volume: 75968900,
    close: 59.37,
  },
  {
    date: '15/03/2013',
    volume: 160990200,
    close: 60.91,
  },
  {
    date: '18/03/2013',
    volume: 151549300,
    close: 62.56,
  },
  {
    date: '19/03/2013',
    volume: 131693800,
    close: 62.39,
  },
  {
    date: '20/03/2013',
    volume: 77165200,
    close: 62.06,
  },
  {
    date: '21/03/2013',
    volume: 95813900,
    close: 62.15,
  },
  {
    date: '22/03/2013',
    volume: 98776300,
    close: 63.41,
  },
  {
    date: '25/03/2013',
    volume: 125283900,
    close: 63.64,
  },
  {
    date: '26/03/2013',
    volume: 73573500,
    close: 63.31,
  },
  {
    date: '27/03/2013',
    volume: 82809300,
    close: 62.06,
  },
  {
    date: '28/03/2013',
    volume: 110709900,
    close: 60.77,
  },
  {
    date: '01/04/2013',
    volume: 97433000,
    close: 58.88,
  },
  {
    date: '02/04/2013',
    volume: 132379800,
    close: 59,
  },
  {
    date: '03/04/2013',
    volume: 90804000,
    close: 59.3,
  },
  {
    date: '04/04/2013',
    volume: 89611900,
    close: 58.72,
  },
  {
    date: '05/04/2013',
    volume: 95923800,
    close: 58.1,
  },
  {
    date: '08/04/2013',
    volume: 75207300,
    close: 58.51,
  },
  {
    date: '09/04/2013',
    volume: 76653500,
    close: 58.62,
  },
  {
    date: '10/04/2013',
    volume: 93982000,
    close: 59.81,
  },
  {
    date: '11/04/2013',
    volume: 82091100,
    close: 59.62,
  },
  {
    date: '12/04/2013',
    volume: 59653300,
    close: 59,
  },
  {
    date: '15/04/2013',
    volume: 79380000,
    close: 57.64,
  },
  {
    date: '16/04/2013',
    volume: 76442800,
    close: 58.51,
  },
  {
    date: '17/04/2013',
    volume: 236264000,
    close: 55.3,
  },
  {
    date: '18/04/2013',
    volume: 166574800,
    close: 53.82,
  },
  {
    date: '19/04/2013',
    volume: 152318600,
    close: 53.61,
  },
  {
    date: '22/04/2013',
    volume: 107480100,
    close: 54.73,
  },
  {
    date: '23/04/2013',
    volume: 166059600,
    close: 55.75,
  },
  {
    date: '24/04/2013',
    volume: 242412800,
    close: 55.66,
  },
  {
    date: '25/04/2013',
    volume: 96209400,
    close: 56.06,
  },
  {
    date: '26/04/2013',
    volume: 191024400,
    close: 57.27,
  },
  {
    date: '29/04/2013',
    volume: 160081600,
    close: 59.05,
  },
  {
    date: '30/04/2013',
    volume: 172884600,
    close: 60.78,
  },
  {
    date: '01/05/2013',
    volume: 126727300,
    close: 60.31,
  },
  {
    date: '02/05/2013',
    volume: 105457100,
    close: 61.16,
  },
  {
    date: '03/05/2013',
    volume: 90325200,
    close: 61.77,
  },
  {
    date: '06/05/2013',
    volume: 124160400,
    close: 63.25,
  },
  {
    date: '07/05/2013',
    volume: 120938300,
    close: 62.96,
  },
  {
    date: '08/05/2013',
    volume: 118149500,
    close: 63.68,
  },
  {
    date: '09/05/2013',
    volume: 99621900,
    close: 63.12,
  },
  {
    date: '10/05/2013',
    volume: 83713000,
    close: 62.6,
  },
  {
    date: '13/05/2013',
    volume: 79237200,
    close: 62.84,
  },
  {
    date: '14/05/2013',
    volume: 111779500,
    close: 61.34,
  },
  {
    date: '15/05/2013',
    volume: 185403400,
    close: 59.26,
  },
  {
    date: '16/05/2013',
    volume: 150801000,
    close: 60.05,
  },
  {
    date: '17/05/2013',
    volume: 106976100,
    close: 59.87,
  },
  {
    date: '20/05/2013',
    volume: 112894600,
    close: 61.21,
  },
  {
    date: '21/05/2013',
    volume: 114005500,
    close: 60.76,
  },
  {
    date: '22/05/2013',
    volume: 110759600,
    close: 60.99,
  },
  {
    date: '23/05/2013',
    volume: 88255300,
    close: 61.1,
  },
  {
    date: '24/05/2013',
    volume: 69041700,
    close: 61.51,
  },
  {
    date: '28/05/2013',
    volume: 96536300,
    close: 61,
  },
  {
    date: '29/05/2013',
    volume: 82644100,
    close: 61.49,
  },
  {
    date: '30/05/2013',
    volume: 88379900,
    close: 62.4,
  },
  {
    date: '31/05/2013',
    volume: 96075700,
    close: 62.15,
  },
  {
    date: '03/06/2013',
    volume: 93088100,
    close: 62.28,
  },
  {
    date: '04/06/2013',
    volume: 73182200,
    close: 62.09,
  },
  {
    date: '05/06/2013',
    volume: 72647400,
    close: 61.51,
  },
  {
    date: '06/06/2013',
    volume: 104233500,
    close: 60.59,
  },
  {
    date: '07/06/2013',
    volume: 101133900,
    close: 61.05,
  },
  {
    date: '10/06/2013',
    volume: 112538300,
    close: 60.65,
  },
  {
    date: '11/06/2013',
    volume: 71528100,
    close: 60.47,
  },
  {
    date: '12/06/2013',
    volume: 66306800,
    close: 59.72,
  },
  {
    date: '13/06/2013',
    volume: 71458100,
    close: 60.24,
  },
  {
    date: '14/06/2013',
    volume: 67966500,
    close: 59.43,
  },
  {
    date: '17/06/2013',
    volume: 64853600,
    close: 59.7,
  },
  {
    date: '18/06/2013',
    volume: 48756400,
    close: 59.67,
  },
  {
    date: '19/06/2013',
    volume: 77735000,
    close: 58.45,
  },
  {
    date: '20/06/2013',
    volume: 89327700,
    close: 57.6,
  },
  {
    date: '21/06/2013',
    volume: 120279600,
    close: 57.14,
  },
  {
    date: '24/06/2013',
    volume: 120186500,
    close: 55.63,
  },
  {
    date: '25/06/2013',
    volume: 78540700,
    close: 55.64,
  },
  {
    date: '26/06/2013',
    volume: 91931000,
    close: 55.01,
  },
  {
    date: '27/06/2013',
    volume: 84311500,
    close: 54.42,
  },
  {
    date: '28/06/2013',
    volume: 144629100,
    close: 54.8,
  },
  {
    date: '01/07/2013',
    volume: 97763400,
    close: 56.55,
  },
  {
    date: '02/07/2013',
    volume: 117466300,
    close: 57.83,
  },
  {
    date: '03/07/2013',
    volume: 60232200,
    close: 58.15,
  },
  {
    date: '05/07/2013',
    volume: 68506200,
    close: 57.68,
  },
  {
    date: '08/07/2013',
    volume: 74534600,
    close: 57.36,
  },
  {
    date: '09/07/2013',
    volume: 88146100,
    close: 58.36,
  },
  {
    date: '10/07/2013',
    volume: 70351400,
    close: 58.14,
  },
  {
    date: '11/07/2013',
    volume: 81573100,
    close: 59.05,
  },
  {
    date: '12/07/2013',
    volume: 69890800,
    close: 58.94,
  },
  {
    date: '15/07/2013',
    volume: 60479300,
    close: 59.07,
  },
  {
    date: '16/07/2013',
    volume: 54134500,
    close: 59.45,
  },
  {
    date: '17/07/2013',
    volume: 49747600,
    close: 59.46,
  },
  {
    date: '18/07/2013',
    volume: 54719700,
    close: 59.66,
  },
  {
    date: '19/07/2013',
    volume: 67180400,
    close: 58.72,
  },
  {
    date: '22/07/2013',
    volume: 51949100,
    close: 58.91,
  },
  {
    date: '23/07/2013',
    volume: 92348900,
    close: 57.9,
  },
  {
    date: '24/07/2013',
    volume: 147984200,
    close: 60.87,
  },
  {
    date: '25/07/2013',
    volume: 57373400,
    close: 60.6,
  },
  {
    date: '26/07/2013',
    volume: 50038100,
    close: 60.94,
  },
  {
    date: '29/07/2013',
    volume: 62014400,
    close: 61.88,
  },
  {
    date: '30/07/2013',
    volume: 77355600,
    close: 62.64,
  },
  {
    date: '31/07/2013',
    volume: 80739400,
    close: 62.53,
  },
  {
    date: '01/08/2013',
    volume: 51562700,
    close: 63.11,
  },
  {
    date: '02/08/2013',
    volume: 68695900,
    close: 63.92,
  },
  {
    date: '05/08/2013',
    volume: 79713900,
    close: 64.87,
  },
  {
    date: '06/08/2013',
    volume: 83714400,
    close: 64.29,
  },
  {
    date: '07/08/2013',
    volume: 74714500,
    close: 64.25,
  },
  {
    date: '08/08/2013',
    volume: 63944300,
    close: 64.13,
  },
  {
    date: '09/08/2013',
    volume: 66716300,
    close: 63.21,
  },
  {
    date: '12/08/2013',
    volume: 91108500,
    close: 65.01,
  },
  {
    date: '13/08/2013',
    volume: 220485300,
    close: 68.1,
  },
  {
    date: '14/08/2013',
    volume: 189093100,
    close: 69.34,
  },
  {
    date: '15/08/2013',
    volume: 122573500,
    close: 69.26,
  },
  {
    date: '16/08/2013',
    volume: 90576500,
    close: 69.87,
  },
  {
    date: '19/08/2013',
    volume: 127629600,
    close: 70.63,
  },
  {
    date: '20/08/2013',
    volume: 89672100,
    close: 69.7,
  },
  {
    date: '21/08/2013',
    volume: 83969900,
    close: 69.88,
  },
  {
    date: '22/08/2013',
    volume: 61051900,
    close: 69.96,
  },
  {
    date: '23/08/2013',
    volume: 55682900,
    close: 69.69,
  },
  {
    date: '26/08/2013',
    volume: 82741400,
    close: 69.96,
  },
  {
    date: '27/08/2013',
    volume: 106047200,
    close: 67.96,
  },
  {
    date: '28/08/2013',
    volume: 76902000,
    close: 68.28,
  },
  {
    date: '29/08/2013',
    volume: 59914400,
    close: 68.4,
  },
  {
    date: '30/08/2013',
    volume: 68074300,
    close: 67.77,
  },
  {
    date: '03/09/2013',
    volume: 82982200,
    close: 67.96,
  },
  {
    date: '04/09/2013',
    volume: 86258200,
    close: 69.37,
  },
  {
    date: '05/09/2013',
    volume: 59091900,
    close: 68.89,
  },
  {
    date: '06/09/2013',
    volume: 89881400,
    close: 69.3,
  },
  {
    date: '09/09/2013',
    volume: 85171800,
    close: 70.41,
  },
  {
    date: '10/09/2013',
    volume: 185798900,
    close: 68.8,
  },
  {
    date: '11/09/2013',
    volume: 224674100,
    close: 65.06,
  },
  {
    date: '12/09/2013',
    volume: 101012800,
    close: 65.75,
  },
  {
    date: '13/09/2013',
    volume: 74708900,
    close: 64.67,
  },
  {
    date: '16/09/2013',
    volume: 135926700,
    close: 62.61,
  },
  {
    date: '17/09/2013',
    volume: 99845200,
    close: 63.34,
  },
  {
    date: '18/09/2013',
    volume: 114215500,
    close: 64.64,
  },
  {
    date: '19/09/2013',
    volume: 101135300,
    close: 65.7,
  },
  {
    date: '20/09/2013',
    volume: 174825700,
    close: 65.02,
  },
  {
    date: '23/09/2013',
    volume: 190526700,
    close: 68.25,
  },
  {
    date: '24/09/2013',
    volume: 91086100,
    close: 68.03,
  },
  {
    date: '25/09/2013',
    volume: 79239300,
    close: 66.98,
  },
  {
    date: '26/09/2013',
    volume: 59305400,
    close: 67.63,
  },
  {
    date: '27/09/2013',
    volume: 57010100,
    close: 67.15,
  },
  {
    date: '30/09/2013',
    volume: 65039100,
    close: 66.32,
  },
  {
    date: '01/10/2013',
    volume: 88470900,
    close: 67.88,
  },
  {
    date: '02/10/2013',
    volume: 72296000,
    close: 68.1,
  },
  {
    date: '03/10/2013',
    volume: 80688300,
    close: 67.24,
  },
  {
    date: '04/10/2013',
    volume: 64717100,
    close: 67.19,
  },
  {
    date: '07/10/2013',
    volume: 78073100,
    close: 67.85,
  },
  {
    date: '08/10/2013',
    volume: 72729300,
    close: 66.9,
  },
  {
    date: '09/10/2013',
    volume: 75431300,
    close: 67.68,
  },
  {
    date: '10/10/2013',
    volume: 69650700,
    close: 68.11,
  },
  {
    date: '11/10/2013',
    volume: 66934700,
    close: 68.55,
  },
  {
    date: '14/10/2013',
    volume: 65474500,
    close: 69,
  },
  {
    date: '15/10/2013',
    volume: 80018400,
    close: 69.37,
  },
  {
    date: '16/10/2013',
    volume: 62775300,
    close: 69.7,
  },
  {
    date: '17/10/2013',
    volume: 63398300,
    close: 70.18,
  },
  {
    date: '18/10/2013',
    volume: 72635500,
    close: 70.79,
  },
  {
    date: '21/10/2013',
    volume: 99526700,
    close: 72.52,
  },
  {
    date: '22/10/2013',
    volume: 133515900,
    close: 72.31,
  },
  {
    date: '23/10/2013',
    volume: 78430800,
    close: 73.02,
  },
  {
    date: '24/10/2013',
    volume: 96191200,
    close: 73.99,
  },
  {
    date: '25/10/2013',
    volume: 84448000,
    close: 73.16,
  },
  {
    date: '28/10/2013',
    volume: 137610200,
    close: 73.71,
  },
  {
    date: '29/10/2013',
    volume: 158951800,
    close: 71.87,
  },
  {
    date: '30/10/2013',
    volume: 88540900,
    close: 73.01,
  },
  {
    date: '31/10/2013',
    volume: 68924100,
    close: 72.71,
  },
  {
    date: '01/11/2013',
    volume: 68722500,
    close: 72.34,
  },
  {
    date: '04/11/2013',
    volume: 61156900,
    close: 73.27,
  },
  {
    date: '05/11/2013',
    volume: 66303300,
    close: 73.09,
  },
  {
    date: '06/11/2013',
    volume: 55843900,
    close: 72.88,
  },
  {
    date: '07/11/2013',
    volume: 65655100,
    close: 71.7,
  },
  {
    date: '08/11/2013',
    volume: 69829200,
    close: 72.83,
  },
  {
    date: '11/11/2013',
    volume: 56863100,
    close: 72.62,
  },
  {
    date: '12/11/2013',
    volume: 51069200,
    close: 72.76,
  },
  {
    date: '13/11/2013',
    volume: 49305200,
    close: 72.84,
  },
  {
    date: '14/11/2013',
    volume: 70604800,
    close: 73.9,
  },
  {
    date: '15/11/2013',
    volume: 79480100,
    close: 73.45,
  },
  {
    date: '18/11/2013',
    volume: 61236000,
    close: 72.56,
  },
  {
    date: '19/11/2013',
    volume: 52234700,
    close: 72.69,
  },
  {
    date: '20/11/2013',
    volume: 48479200,
    close: 72.06,
  },
  {
    date: '21/11/2013',
    volume: 65506700,
    close: 72.91,
  },
  {
    date: '22/11/2013',
    volume: 55931400,
    close: 72.73,
  },
  {
    date: '25/11/2013',
    volume: 57327900,
    close: 73.28,
  },
  {
    date: '26/11/2013',
    volume: 100345700,
    close: 74.63,
  },
  {
    date: '27/11/2013',
    volume: 90862100,
    close: 76.39,
  },
  {
    date: '29/11/2013',
    volume: 79531900,
    close: 77.8,
  },
  {
    date: '02/12/2013',
    volume: 118136200,
    close: 77.12,
  },
  {
    date: '03/12/2013',
    volume: 112742000,
    close: 79.24,
  },
  {
    date: '04/12/2013',
    volume: 94452400,
    close: 79.05,
  },
  {
    date: '05/12/2013',
    volume: 111895000,
    close: 79.46,
  },
  {
    date: '06/12/2013',
    volume: 86088100,
    close: 78.35,
  },
  {
    date: '09/12/2013',
    volume: 80123400,
    close: 79.25,
  },
  {
    date: '10/12/2013',
    volume: 69567400,
    close: 79.13,
  },
  {
    date: '11/12/2013',
    volume: 89929700,
    close: 78.54,
  },
  {
    date: '12/12/2013',
    volume: 65572500,
    close: 78.43,
  },
  {
    date: '13/12/2013',
    volume: 83205500,
    close: 77.57,
  },
  {
    date: '16/12/2013',
    volume: 70648200,
    close: 78,
  },
  {
    date: '17/12/2013',
    volume: 57475600,
    close: 77.65,
  },
  {
    date: '18/12/2013',
    volume: 141465800,
    close: 77.06,
  },
  {
    date: '19/12/2013',
    volume: 80077200,
    close: 76.18,
  },
  {
    date: '20/12/2013',
    volume: 109103400,
    close: 76.81,
  },
  {
    date: '23/12/2013',
    volume: 125326600,
    close: 79.76,
  },
  {
    date: '24/12/2013',
    volume: 41888700,
    close: 79.42,
  },
  {
    date: '26/12/2013',
    volume: 51002000,
    close: 78.9,
  },
  {
    date: '27/12/2013',
    volume: 56471100,
    close: 78.36,
  },
  {
    date: '30/12/2013',
    volume: 63407400,
    close: 77.58,
  },
  {
    date: '31/12/2013',
    volume: 55771100,
    close: 78.49,
  },
  {
    date: '02/01/2014',
    volume: 58671200,
    close: 77.39,
  },
  {
    date: '03/01/2014',
    volume: 98116900,
    close: 75.69,
  },
  {
    date: '06/01/2014',
    volume: 103152700,
    close: 76.1,
  },
  {
    date: '07/01/2014',
    volume: 79302300,
    close: 75.56,
  },
  {
    date: '08/01/2014',
    volume: 64632400,
    close: 76.04,
  },
  {
    date: '09/01/2014',
    volume: 69787200,
    close: 75.07,
  },
  {
    date: '10/01/2014',
    volume: 76244000,
    close: 74.57,
  },
  {
    date: '13/01/2014',
    volume: 94623200,
    close: 74.96,
  },
  {
    date: '14/01/2014',
    volume: 83140400,
    close: 76.45,
  },
  {
    date: '15/01/2014',
    volume: 97909700,
    close: 77.98,
  },
  {
    date: '16/01/2014',
    volume: 57319500,
    close: 77.55,
  },
  {
    date: '17/01/2014',
    volume: 106684900,
    close: 75.65,
  },
  {
    date: '21/01/2014',
    volume: 82131700,
    close: 76.82,
  },
  {
    date: '22/01/2014',
    volume: 94996300,
    close: 77.16,
  },
  {
    date: '23/01/2014',
    volume: 100809800,
    close: 77.82,
  },
  {
    date: '24/01/2014',
    volume: 107338700,
    close: 76.4,
  },
  {
    date: '27/01/2014',
    volume: 138719700,
    close: 77.02,
  },
  {
    date: '28/01/2014',
    volume: 266380800,
    close: 70.87,
  },
  {
    date: '29/01/2014',
    volume: 125702500,
    close: 70.06,
  },
  {
    date: '30/01/2014',
    volume: 169625400,
    close: 69.93,
  },
  {
    date: '31/01/2014',
    volume: 116199300,
    close: 70.04,
  },
  {
    date: '03/02/2014',
    volume: 100366000,
    close: 70.17,
  },
  {
    date: '04/02/2014',
    volume: 94170300,
    close: 71.19,
  },
  {
    date: '05/02/2014',
    volume: 82086200,
    close: 71.72,
  },
  {
    date: '06/02/2014',
    volume: 64441300,
    close: 72.14,
  },
  {
    date: '07/02/2014',
    volume: 92570100,
    close: 73.15,
  },
  {
    date: '10/02/2014',
    volume: 86389800,
    close: 74.46,
  },
  {
    date: '11/02/2014',
    volume: 70564200,
    close: 75.44,
  },
  {
    date: '12/02/2014',
    volume: 77025200,
    close: 75.43,
  },
  {
    date: '13/02/2014',
    volume: 76849500,
    close: 76.63,
  },
  {
    date: '14/02/2014',
    volume: 68231100,
    close: 76.57,
  },
  {
    date: '18/02/2014',
    volume: 65062900,
    close: 76.85,
  },
  {
    date: '19/02/2014',
    volume: 78442000,
    close: 75.63,
  },
  {
    date: '20/02/2014',
    volume: 76464500,
    close: 74.76,
  },
  {
    date: '21/02/2014',
    volume: 69696200,
    close: 73.93,
  },
  {
    date: '24/02/2014',
    volume: 72227400,
    close: 74.25,
  },
  {
    date: '25/02/2014',
    volume: 57988000,
    close: 73.48,
  },
  {
    date: '26/02/2014',
    volume: 69054300,
    close: 72.82,
  },
  {
    date: '27/02/2014',
    volume: 75470500,
    close: 74.27,
  },
  {
    date: '28/02/2014',
    volume: 92992200,
    close: 74.07,
  },
  {
    date: '03/03/2014',
    volume: 59695300,
    close: 74.28,
  },
  {
    date: '04/03/2014',
    volume: 64785000,
    close: 74.77,
  },
  {
    date: '05/03/2014',
    volume: 50015700,
    close: 74.93,
  },
  {
    date: '06/03/2014',
    volume: 46372200,
    close: 74.7,
  },
  {
    date: '07/03/2014',
    volume: 55182400,
    close: 74.66,
  },
  {
    date: '10/03/2014',
    volume: 44646000,
    close: 74.73,
  },
  {
    date: '11/03/2014',
    volume: 69806100,
    close: 75.45,
  },
  {
    date: '12/03/2014',
    volume: 49831600,
    close: 75.53,
  },
  {
    date: '13/03/2014',
    volume: 64435700,
    close: 74.69,
  },
  {
    date: '14/03/2014',
    volume: 59299800,
    close: 73.85,
  },
  {
    date: '17/03/2014',
    volume: 49886200,
    close: 74.14,
  },
  {
    date: '18/03/2014',
    volume: 52411800,
    close: 74.79,
  },
  {
    date: '19/03/2014',
    volume: 56189000,
    close: 74.77,
  },
  {
    date: '20/03/2014',
    volume: 52099600,
    close: 74.41,
  },
  {
    date: '21/03/2014',
    volume: 93511600,
    close: 75,
  },
  {
    date: '24/03/2014',
    volume: 88925200,
    close: 75.89,
  },
  {
    date: '25/03/2014',
    volume: 70573300,
    close: 76.71,
  },
  {
    date: '26/03/2014',
    volume: 74942000,
    close: 75.97,
  },
  {
    date: '27/03/2014',
    volume: 55507900,
    close: 75.65,
  },
  {
    date: '28/03/2014',
    volume: 50141000,
    close: 75.56,
  },
  {
    date: '31/03/2014',
    volume: 42167300,
    close: 75.55,
  },
  {
    date: '01/04/2014',
    volume: 50190000,
    close: 76.24,
  },
  {
    date: '02/04/2014',
    volume: 45105200,
    close: 76.36,
  },
  {
    date: '03/04/2014',
    volume: 40586000,
    close: 75.83,
  },
  {
    date: '04/04/2014',
    volume: 68812800,
    close: 74.85,
  },
  {
    date: '07/04/2014',
    volume: 72462600,
    close: 73.68,
  },
  {
    date: '08/04/2014',
    volume: 60972100,
    close: 73.67,
  },
  {
    date: '09/04/2014',
    volume: 51542400,
    close: 74.64,
  },
  {
    date: '10/04/2014',
    volume: 59913000,
    close: 73.68,
  },
  {
    date: '11/04/2014',
    volume: 67929400,
    close: 73.14,
  },
  {
    date: '14/04/2014',
    volume: 51418500,
    close: 73.43,
  },
  {
    date: '15/04/2014',
    volume: 66622500,
    close: 72.9,
  },
  {
    date: '16/04/2014',
    volume: 53691400,
    close: 73.05,
  },
  {
    date: '17/04/2014',
    volume: 71083600,
    close: 73.89,
  },
  {
    date: '21/04/2014',
    volume: 45637200,
    close: 74.76,
  },
  {
    date: '22/04/2014',
    volume: 50640800,
    close: 74.84,
  },
  {
    date: '23/04/2014',
    volume: 98735000,
    close: 73.86,
  },
  {
    date: '24/04/2014',
    volume: 189977900,
    close: 79.91,
  },
  {
    date: '25/04/2014',
    volume: 97568800,
    close: 80.5,
  },
  {
    date: '28/04/2014',
    volume: 167371400,
    close: 83.62,
  },
  {
    date: '29/04/2014',
    volume: 84344400,
    close: 83.37,
  },
  {
    date: '30/04/2014',
    volume: 114160200,
    close: 83.06,
  },
  {
    date: '01/05/2014',
    volume: 61012000,
    close: 83.25,
  },
  {
    date: '02/05/2014',
    volume: 47878600,
    close: 83.41,
  },
  {
    date: '05/05/2014',
    volume: 71766800,
    close: 84.59,
  },
  {
    date: '06/05/2014',
    volume: 93641100,
    close: 83.66,
  },
  {
    date: '07/05/2014',
    volume: 70716100,
    close: 83.37,
  },
  {
    date: '08/05/2014',
    volume: 57574300,
    close: 83.22,
  },
  {
    date: '09/05/2014',
    volume: 72899400,
    close: 82.88,
  },
  {
    date: '12/05/2014',
    volume: 53302200,
    close: 83.91,
  },
  {
    date: '13/05/2014',
    volume: 39934300,
    close: 84.04,
  },
  {
    date: '14/05/2014',
    volume: 41601000,
    close: 84.05,
  },
  {
    date: '15/05/2014',
    volume: 57711500,
    close: 83.34,
  },
  {
    date: '16/05/2014',
    volume: 69064100,
    close: 84.57,
  },
  {
    date: '19/05/2014',
    volume: 79438800,
    close: 85.57,
  },
  {
    date: '20/05/2014',
    volume: 58709000,
    close: 85.59,
  },
  {
    date: '21/05/2014',
    volume: 49214900,
    close: 85.81,
  },
  {
    date: '22/05/2014',
    volume: 50190000,
    close: 85.95,
  },
  {
    date: '23/05/2014',
    volume: 58052400,
    close: 86.92,
  },
  {
    date: '27/05/2014',
    volume: 87216500,
    close: 88.55,
  },
  {
    date: '28/05/2014',
    volume: 78870400,
    close: 88.32,
  },
  {
    date: '29/05/2014',
    volume: 94118500,
    close: 89.93,
  },
  {
    date: '30/05/2014',
    volume: 141005200,
    close: 89.59,
  },
  {
    date: '02/06/2014',
    volume: 92337700,
    close: 88.98,
  },
  {
    date: '03/06/2014',
    volume: 73177300,
    close: 90.24,
  },
  {
    date: '04/06/2014',
    volume: 83870500,
    close: 91.27,
  },
  {
    date: '05/06/2014',
    volume: 75951400,
    close: 91.62,
  },
  {
    date: '06/06/2014',
    volume: 87484600,
    close: 91.37,
  },
  {
    date: '09/06/2014',
    volume: 75415000,
    close: 92.83,
  },
  {
    date: '10/06/2014',
    volume: 62777000,
    close: 93.38,
  },
  {
    date: '11/06/2014',
    volume: 45681000,
    close: 92.99,
  },
  {
    date: '12/06/2014',
    volume: 54749000,
    close: 91.44,
  },
  {
    date: '13/06/2014',
    volume: 54525000,
    close: 90.44,
  },
  {
    date: '16/06/2014',
    volume: 35561000,
    close: 91.35,
  },
  {
    date: '17/06/2014',
    volume: 29726000,
    close: 91.23,
  },
  {
    date: '18/06/2014',
    volume: 33514000,
    close: 91.33,
  },
  {
    date: '19/06/2014',
    volume: 35528000,
    close: 91.01,
  },
  {
    date: '20/06/2014',
    volume: 100898000,
    close: 90.07,
  },
  {
    date: '23/06/2014',
    volume: 43694000,
    close: 89.99,
  },
  {
    date: '24/06/2014',
    volume: 39036000,
    close: 89.45,
  },
  {
    date: '25/06/2014',
    volume: 36869000,
    close: 89.52,
  },
  {
    date: '26/06/2014',
    volume: 32629000,
    close: 90.06,
  },
  {
    date: '27/06/2014',
    volume: 64029000,
    close: 91.13,
  },
  {
    date: '30/06/2014',
    volume: 49589000,
    close: 92.07,
  },
  {
    date: '01/07/2014',
    volume: 38223000,
    close: 92.66,
  },
  {
    date: '02/07/2014',
    volume: 28465000,
    close: 92.62,
  },
  {
    date: '03/07/2014',
    volume: 22891800,
    close: 93.16,
  },
  {
    date: '07/07/2014',
    volume: 56468000,
    close: 95.08,
  },
  {
    date: '08/07/2014',
    volume: 65222000,
    close: 94.47,
  },
  {
    date: '09/07/2014',
    volume: 36436000,
    close: 94.51,
  },
  {
    date: '10/07/2014',
    volume: 39686000,
    close: 94.16,
  },
  {
    date: '11/07/2014',
    volume: 34018000,
    close: 94.34,
  },
  {
    date: '14/07/2014',
    volume: 42810000,
    close: 95.56,
  },
  {
    date: '15/07/2014',
    volume: 45477900,
    close: 94.44,
  },
  {
    date: '16/07/2014',
    volume: 53502000,
    close: 93.9,
  },
  {
    date: '17/07/2014',
    volume: 57298000,
    close: 92.23,
  },
  {
    date: '18/07/2014',
    volume: 49988000,
    close: 93.56,
  },
  {
    date: '21/07/2014',
    volume: 39079000,
    close: 93.07,
  },
  {
    date: '22/07/2014',
    volume: 55197000,
    close: 93.84,
  },
  {
    date: '23/07/2014',
    volume: 92918000,
    close: 96.29,
  },
  {
    date: '24/07/2014',
    volume: 45729000,
    close: 96.13,
  },
  {
    date: '25/07/2014',
    volume: 43469000,
    close: 96.77,
  },
  {
    date: '28/07/2014',
    volume: 55318000,
    close: 98.1,
  },
  {
    date: '29/07/2014',
    volume: 43143000,
    close: 97.47,
  },
  {
    date: '30/07/2014',
    volume: 33010000,
    close: 97.24,
  },
  {
    date: '31/07/2014',
    volume: 56843000,
    close: 94.72,
  },
  {
    date: '01/08/2014',
    volume: 48511000,
    close: 95.24,
  },
  {
    date: '04/08/2014',
    volume: 39958000,
    close: 94.71,
  },
  {
    date: '05/08/2014',
    volume: 55933000,
    close: 94.24,
  },
  {
    date: '06/08/2014',
    volume: 38558000,
    close: 94.08,
  },
  {
    date: '07/08/2014',
    volume: 46711000,
    close: 94.07,
  },
  {
    date: '08/08/2014',
    volume: 41865000,
    close: 94.33,
  },
  {
    date: '11/08/2014',
    volume: 36585000,
    close: 95.58,
  },
  {
    date: '12/08/2014',
    volume: 33795000,
    close: 95.56,
  },
  {
    date: '13/08/2014',
    volume: 31916000,
    close: 96.82,
  },
  {
    date: '14/08/2014',
    volume: 28116000,
    close: 97.08,
  },
  {
    date: '15/08/2014',
    volume: 48951000,
    close: 97.56,
  },
  {
    date: '18/08/2014',
    volume: 47572000,
    close: 98.73,
  },
  {
    date: '19/08/2014',
    volume: 69399000,
    close: 100.1,
  },
  {
    date: '20/08/2014',
    volume: 52699000,
    close: 100.14,
  },
  {
    date: '21/08/2014',
    volume: 33478000,
    close: 100.15,
  },
  {
    date: '22/08/2014',
    volume: 44184000,
    close: 100.88,
  },
  {
    date: '25/08/2014',
    volume: 40270000,
    close: 101.1,
  },
  {
    date: '26/08/2014',
    volume: 33152000,
    close: 100.45,
  },
  {
    date: '27/08/2014',
    volume: 52369000,
    close: 101.69,
  },
  {
    date: '28/08/2014',
    volume: 68460000,
    close: 101.81,
  },
  {
    date: '29/08/2014',
    volume: 44595000,
    close: 102.06,
  },
  {
    date: '02/09/2014',
    volume: 53564000,
    close: 102.85,
  },
  {
    date: '03/09/2014',
    volume: 125421000,
    close: 98.51,
  },
  {
    date: '04/09/2014',
    volume: 85718000,
    close: 97.7,
  },
  {
    date: '05/09/2014',
    volume: 58457000,
    close: 98.54,
  },
  {
    date: '08/09/2014',
    volume: 46356700,
    close: 97.94,
  },
  {
    date: '09/09/2014',
    volume: 189846300,
    close: 97.57,
  },
  {
    date: '10/09/2014',
    volume: 100869600,
    close: 100.56,
  },
  {
    date: '11/09/2014',
    volume: 62353100,
    close: 100.99,
  },
  {
    date: '12/09/2014',
    volume: 64096900,
    close: 101.22,
  },
  {
    date: '15/09/2014',
    volume: 61316500,
    close: 101.19,
  },
  {
    date: '16/09/2014',
    volume: 66908100,
    close: 100.42,
  },
  {
    date: '17/09/2014',
    volume: 60926500,
    close: 101.14,
  },
  {
    date: '18/09/2014',
    volume: 37299400,
    close: 101.35,
  },
  {
    date: '19/09/2014',
    volume: 70902400,
    close: 100.52,
  },
  {
    date: '22/09/2014',
    volume: 52788400,
    close: 100.62,
  },
  {
    date: '23/09/2014',
    volume: 63402200,
    close: 102.2,
  },
  {
    date: '24/09/2014',
    volume: 60171800,
    close: 101.31,
  },
  {
    date: '25/09/2014',
    volume: 100092000,
    close: 97.45,
  },
  {
    date: '26/09/2014',
    volume: 62370500,
    close: 100.32,
  },
  {
    date: '29/09/2014',
    volume: 49766300,
    close: 99.68,
  },
  {
    date: '30/09/2014',
    volume: 55264100,
    close: 100.32,
  },
  {
    date: '01/10/2014',
    volume: 51491300,
    close: 98.75,
  },
  {
    date: '02/10/2014',
    volume: 47757800,
    close: 99.47,
  },
  {
    date: '03/10/2014',
    volume: 43469600,
    close: 99.19,
  },
  {
    date: '06/10/2014',
    volume: 37051200,
    close: 99.19,
  },
  {
    date: '07/10/2014',
    volume: 42094200,
    close: 98.32,
  },
  {
    date: '08/10/2014',
    volume: 57404700,
    close: 100.36,
  },
  {
    date: '09/10/2014',
    volume: 77376500,
    close: 100.58,
  },
  {
    date: '10/10/2014',
    volume: 66331600,
    close: 100.3,
  },
  {
    date: '13/10/2014',
    volume: 53583400,
    close: 99.38,
  },
  {
    date: '14/10/2014',
    volume: 63688600,
    close: 98.32,
  },
  {
    date: '15/10/2014',
    volume: 100933600,
    close: 97.12,
  },
  {
    date: '16/10/2014',
    volume: 72154500,
    close: 95.84,
  },
  {
    date: '17/10/2014',
    volume: 68179700,
    close: 97.25,
  },
  {
    date: '20/10/2014',
    volume: 77517300,
    close: 99.33,
  },
  {
    date: '21/10/2014',
    volume: 94623900,
    close: 102.03,
  },
  {
    date: '22/10/2014',
    volume: 68263100,
    close: 102.55,
  },
  {
    date: '23/10/2014',
    volume: 71074700,
    close: 104.38,
  },
  {
    date: '24/10/2014',
    volume: 47053900,
    close: 104.77,
  },
  {
    date: '27/10/2014',
    volume: 34187700,
    close: 104.66,
  },
  {
    date: '28/10/2014',
    volume: 47939900,
    close: 106.28,
  },
  {
    date: '29/10/2014',
    volume: 52687900,
    close: 106.88,
  },
  {
    date: '30/10/2014',
    volume: 40654800,
    close: 106.52,
  },
  {
    date: '31/10/2014',
    volume: 44639300,
    close: 107.53,
  },
  {
    date: '03/11/2014',
    volume: 52282600,
    close: 108.93,
  },
  {
    date: '04/11/2014',
    volume: 41574400,
    close: 108.13,
  },
  {
    date: '05/11/2014',
    volume: 37435900,
    close: 108.39,
  },
  {
    date: '06/11/2014',
    volume: 34968500,
    close: 108.7,
  },
  {
    date: '07/11/2014',
    volume: 33691500,
    close: 109.01,
  },
  {
    date: '10/11/2014',
    volume: 27195500,
    close: 108.83,
  },
  {
    date: '11/11/2014',
    volume: 27442300,
    close: 109.7,
  },
  {
    date: '12/11/2014',
    volume: 46942400,
    close: 111.25,
  },
  {
    date: '13/11/2014',
    volume: 59522900,
    close: 112.82,
  },
  {
    date: '14/11/2014',
    volume: 44063600,
    close: 114.18,
  },
  {
    date: '17/11/2014',
    volume: 46746700,
    close: 113.99,
  },
  {
    date: '18/11/2014',
    volume: 44224000,
    close: 115.47,
  },
  {
    date: '19/11/2014',
    volume: 41869200,
    close: 114.67,
  },
  {
    date: '20/11/2014',
    volume: 43395500,
    close: 116.31,
  },
  {
    date: '21/11/2014',
    volume: 57179300,
    close: 116.47,
  },
  {
    date: '24/11/2014',
    volume: 47450800,
    close: 118.63,
  },
  {
    date: '25/11/2014',
    volume: 68840400,
    close: 117.6,
  },
  {
    date: '26/11/2014',
    volume: 40768300,
    close: 119,
  },
  {
    date: '28/11/2014',
    volume: 24814400,
    close: 118.93,
  },
  {
    date: '01/12/2014',
    volume: 83814000,
    close: 115.07,
  },
  {
    date: '02/12/2014',
    volume: 59075100,
    close: 114.63,
  },
  {
    date: '03/12/2014',
    volume: 43063400,
    close: 115.93,
  },
  {
    date: '04/12/2014',
    volume: 42044500,
    close: 115.49,
  },
  {
    date: '05/12/2014',
    volume: 38318900,
    close: 115,
  },
  {
    date: '08/12/2014',
    volume: 57664900,
    close: 112.4,
  },
  {
    date: '09/12/2014',
    volume: 60208000,
    close: 114.12,
  },
  {
    date: '10/12/2014',
    volume: 44565300,
    close: 111.95,
  },
  {
    date: '11/12/2014',
    volume: 41401700,
    close: 111.62,
  },
  {
    date: '12/12/2014',
    volume: 56028100,
    close: 109.73,
  },
  {
    date: '15/12/2014',
    volume: 67218100,
    close: 108.23,
  },
  {
    date: '16/12/2014',
    volume: 60790700,
    close: 106.75,
  },
  {
    date: '17/12/2014',
    volume: 53411800,
    close: 109.41,
  },
  {
    date: '18/12/2014',
    volume: 59006200,
    close: 112.65,
  },
  {
    date: '19/12/2014',
    volume: 85708100,
    close: 111.7,
  },
];
